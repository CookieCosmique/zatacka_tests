/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   update_cam.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/09 14:46:01 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

//TODO transition smooth using ratio
/*static float	update_ratio(float ratio, const float t, const float target)
{
	*//*if (target >= ratio)
	{
		ratio += (target - ratio) * t * 2;
		if (ratio >= target)
			return (target);
	}
	else
	{
		ratio += (target - ratio) * t * 2;
		if (ratio <= target)
			return (target);
	}*/
	/*if (target >= ratio)
	{
		ratio += 1.0f * t * 2;
		if (ratio >= target)
			return (target);
	}
	else
	{
		ratio += -1.0f * t * 2;
		if (ratio <= target)
			return (target);
	}
	return (ratio);
}*/

static void		smooth(float *pos, float *from, float *target, const float t)
{
	pos[0] += (target[0] - from[0]) * t;
	pos[1] += (target[1] - from[1]) * t;
}

void			update_manual_cam(t_cam_type *cam, t_player *p, float *cam_pos,
			float *cam_size, const float t, float *translate)
{
	(void)p;
	(void)t;
	(void)cam_pos;
	cam->pos[0] += translate[0];
	cam->pos[1] += translate[1];
	cam->size[0] = cam_size[0];
	cam->size[1] = cam_size[1];
}

void			update_follow_cam(t_cam_type *cam, t_player *p, float *cam_pos,
			float *cam_size, const float t, float *translate)
{
	(void)t;
	(void)cam_size;
	(void)cam_pos;
	(void)cam_size;
	cam->pos[0] += translate[0];
	cam->pos[1] += translate[1];
	if (p->pos[0] < cam->pos[0] + cam->range * cam->size[0])
		cam->pos[0] = p->pos[0] - cam->size[0] * cam->range;
	if (p->pos[0] > cam->pos[0] + (1 - cam->range) * cam->size[0])
		cam->pos[0] = p->pos[0] - cam->size[0] * (1 - cam->range);
	if (p->pos[1] < cam->pos[1] + cam->range * cam->size[1])
		cam->pos[1] = p->pos[1] - cam->size[1] * cam->range;
	if (p->pos[1] > cam->pos[1] + (1 - cam->range) * cam->size[1])
		cam->pos[1] = p->pos[1] - cam->size[1] * (1 - cam->range);
}

void			update_yolo_cam(t_cam_type *cam, t_player *p, float *cam_pos,
			float *cam_size, const float t, float *translate)
{
	static float	player_pos_cam_s[2] = {0, 0};
	static float	player_dir_s[2] = {0, 0};
	float			player_pos_cam[2];
	float			range;

	(void)cam_pos;
	(void)cam_size;
	(void)translate;

	range = cam->range / (((p->speed - p->speed_min) /
						(p->speed_max - p->speed_min)) + 1.0f);
	if (!player_dir_s[0] && !player_dir_s[1])
	{
		player_dir_s[0] = p->dir[0];
		player_dir_s[1] = p->dir[1];
	}
	else
		smooth(player_dir_s, player_dir_s, p->dir, t);
	player_pos_cam[0] = cam->size[0] - (range * cam->size[0] +
			(player_dir_s[0] + 1.0f) / 2.0f * ((1.0f - range * 2) *
			cam->size[0]));
	player_pos_cam[1] = cam->size[1] - (range * cam->size[1] +
			(player_dir_s[1] + 1.0f) / 2.0f * ((1.0f - range * 2) *
			cam->size[1]));
//	ft_log(LOG_DEBUG, 0, "player_pos_cam: %f, %f", player_pos_cam[0], player_pos_cam[1]);
	if (!player_pos_cam_s[0] && !player_pos_cam_s[1])
	{
		player_pos_cam_s[0] = player_pos_cam[0];
		player_pos_cam_s[1] = player_pos_cam[1];
	}
	else
		smooth(player_pos_cam_s, player_pos_cam_s, player_pos_cam, t);
//	ft_log(LOG_DEBUG, 0, "player_pos_cam_s: %f, %f", player_pos_cam_s[0], player_pos_cam_s[1]);
	cam->pos[0] = p->pos[0] - player_pos_cam_s[0];
	cam->pos[1] = p->pos[1] - player_pos_cam_s[1];
}

static void		translate_cam(t_cam *cam, float t)
{
	cam->translate[0] = 0;
	cam->translate[1] = 0;
	if (cam->input.moves & CAM_MOVE_LEFT)
		cam->translate[0] = -cam->speed * cam->size[0] / 1280.0f * t;
	if (cam->input.moves & CAM_MOVE_RIGHT)
		cam->translate[0] = cam->speed * cam->size[0] / 1280.0f * t;
	if (cam->input.moves & CAM_MOVE_UP)
		cam->translate[1] = cam->speed * cam->size[1] / 720.0f * t;
	if (cam->input.moves & CAM_MOVE_DOWN)
		cam->translate[1] = -cam->speed * cam->size[1] / 720.0f * t;
	if (cam->input.moves & CAM_ZOOM_OUT)
	{
		cam->translate[0] = -cam->size[0] * (exp(t * cam->zoom_speed) - 1) / 2;
		cam->translate[1] = -cam->size[1] * (exp(t * cam->zoom_speed) - 1) / 2;
		cam->size[0] *= exp(t * cam->zoom_speed);
		cam->size[1] *= exp(t * cam->zoom_speed);
	}
	if (cam->input.moves & CAM_ZOOM_IN)
	{
		cam->translate[0] = -cam->size[0] * (exp(-t * cam->zoom_speed) - 1) / 2;
		cam->translate[1] = cam->size[1] * (exp(-t * cam->zoom_speed) - 1) / 2;
		cam->size[0] *= exp(-t * cam->zoom_speed);
		cam->size[1] *= exp(-t * cam->zoom_speed);
	}
}

static void		update_uniforms(t_env *e)
{
	glUseProgram(e->player_sp.shader_program.id);
	glUniform2fv(e->player_sp.id_cam_size, 1, e->cam->size);
	glUniform2fv(e->player_sp.id_cam_pos, 1, e->cam->pos);
	glUseProgram(e->line_sp.shader_program.id);
	glUniform2fv(e->line_sp.id_cam_size, 1, e->cam->size);
	glUniform2fv(e->line_sp.id_cam_pos, 1, e->cam->pos);
	if (!(e->flags & F_NO_BACKGROUND))
	{
		glUseProgram(e->background.background_sp.shader_program.id);
		glUniform2fv(e->background.background_sp.id_cam_size, 1, e->cam->size);
		glUniform2fv(e->background.background_sp.id_cam_pos, 1, e->cam->pos);
	}
}
/*
static float	get_ratio(float ratio)
{
	if (ratio <= 0.5f)
		return (pow(2 * ratio, 2) / 2.0f);
	else
		return (1 - pow(2 * (ratio - 1), 2) / 2.0f);
}
*/
/*
static float	get_ratio(float ratio)
{
	if (ratio <= 0.5f)
		return ((1 - sqrt(1 - pow(2 * ratio, 2))) / 2.0f);
	else if (ratio >= 0.5f)
		return (1 - ((1 - sqrt(1 - pow(2 * (ratio - 1), 2))) / 2.0f));
	else
		return (0.5f);
}
*/

static float	func_duration(float	duration)
{
	return (1 - exp(-duration));
}


/*static float	get_ratio(float ratio)
{
	return (1 - exp(-ratio));
}*/

//TODO clean inputs
void			update_cam(t_env *e, const float t, const float game_speed)
{
	t_cam_type	*cam;
//	float		sum_ratios;
	size_t		i;
	size_t		start_status;

	static float		old_pos[2] = {0.0f, 0.0f};

	translate_cam(e->cam, t);
//	ft_log(LOG_DEBUG, 1, "Camera:");
//	ft_log(LOG_DEBUG, 0, "pos: (%f, %f)", e->cam->pos[0], e->cam->pos[1]);
//	ft_log(LOG_DEBUG, -1, "size: (%f, %f)", e->cam->size[0], e->cam->size[1]);
//	sum_ratios = 0;
	i = 0;
	start_status = e->cam->status;

	while (i < CAM_TYPE_NB)
	{
		cam = e->cam->cam_type[i];
		if (glfwGetKey(e->window, cam->key) == GLFW_PRESS && !cam->pressed)
		{
			//TODO transition
			
			if(i == 0)
			{
				cam->pos[0] = e->cam->pos[0];
				cam->pos[1] = e->cam->pos[1];
				cam->size[0] = e->cam->size[0];
				cam->size[1] = e->cam->size[1];
			}
			e->cam->status = i;
			cam->pressed = 1;
		}
		else if (glfwGetKey(e->window, cam->key) == GLFW_RELEASE)
			cam->pressed = 0;
		/*if ((cam->ratio = update_ratio(cam->ratio, t * game_speed, (e->cam->status == i) ? 1.0f : 0.0f)) &&
				((i > 1) || e->cam->status == i))*/
		//cam->ratio = update_ratio(cam->ratio, t * game_speed, (e->cam->status == i) ? 1.0f : 0.0f);
        cam->update(cam, ((t_player**)e->players->data)[0], e->cam->pos, e->cam->size, t * game_speed, e->cam->translate);
		
		//sum_ratios += get_ratio(cam->ratio);
		i++;
	}

	e->cam->trans_duration += t * game_speed / e->cam->trans_dir_size;

	if(start_status != e->cam->status)
	{
		//ft_log(LOG_ERROR, 0, "CHAGING STATUS : FROM %d TO %d", start_status, e->cam->status);

		e->cam->trans_dir_size = 0.0f;
		i = 0;
		while(i < CAM_TYPE_NB)
		{
			e->cam->trans_start_pos[i] = e->cam->trans_curr_pos[i];
			e->cam->trans_dir[i] = ( (e->cam->status == i) ? 1.0f : 0.0f ) - e->cam->trans_start_pos[i];
			e->cam->trans_dir_size += fabs(pow(e->cam->trans_dir[i], (float) CAM_TYPE_NB));
			i++;
		}
		e->cam->trans_dir_size = pow(e->cam->trans_dir_size, 1.0f / ( (float) CAM_TYPE_NB )) / pow(2.0f, 1.0f/(float) CAM_TYPE_NB);
		e->cam->trans_duration = 0.0f;

//		ft_log(LOG_DEBUG, 0, "trand dir size : %f ", e->cam->trans_dir_size);
	}


	e->cam->pos[0] = 0;
	e->cam->pos[1] = 0;
	i = 0;
	/*ft_log(LOG_DEBUG, 0, "duration : %f ; func duration : %f", e->cam->trans_duration, func_duration(e->cam->trans_duration));
	ft_log(LOG_DEBUG, 0, "dir : (%f, %f, %f)", e->cam->trans_dir[0], e->cam->trans_dir[1], e->cam->trans_dir[2]);
	ft_log(LOG_DEBUG, 0, "start pos : (%f, %f, %f)", e->cam->trans_start_pos[0], e->cam->trans_start_pos[1], e->cam->trans_start_pos[2]);*/
	
	while (i < CAM_TYPE_NB)
	{
//		if (i == 0)
//			ft_log(LOG_DEBUG, 0, "f(%f) = %f", e->cam->cam_type[i]->ratio, get_ratio(e->cam->cam_type[i]->ratio));
		//ft_log(LOG_DEBUG, 1, "Camera %d:", i);
		/*e->cam->pos[0] += e->cam->cam_type[i]->pos[0] * get_ratio(e->cam->cam_type[i]->ratio) / sum_ratios;
		e->cam->pos[1] += e->cam->cam_type[i]->pos[1] * get_ratio(e->cam->cam_type[i]->ratio) / sum_ratios;*/

		e->cam->trans_curr_pos[i] = e->cam->trans_dir[i] * func_duration(e->cam->trans_duration) + e->cam->trans_start_pos[i];
//		e->cam->trans_curr_pos[i] = e->cam->trans_dir[i] * 1 + e->cam->trans_start_pos[i];
//		ft_log(LOG_DEBUG, 0, "curr pos %d : %f", i, e->cam->trans_curr_pos[i]);
//		ft_log(LOG_DEBUG, 0, "pos: (%f, %f)", e->cam->cam_type[i]->pos[0], e->cam->cam_type[i]->pos[1]);

		e->cam->pos[0] += e->cam->cam_type[i]->pos[0] * e->cam->trans_curr_pos[i];
		e->cam->pos[1] += e->cam->cam_type[i]->pos[1] * e->cam->trans_curr_pos[i];

		i++;
	}

	/*ft_log(LOG_DEBUG, 0, "cam final pos : (%f, %f)", e->cam->pos[0], e->cam->pos[1]);
	ft_log(LOG_DEBUG, 0, "//////CAM DIFFERENTIAL : (%f, %f)", e->cam->pos[0] - old_pos[0], e->cam->pos[1] - old_pos[1]);*/

	old_pos[0] = e->cam->pos[0];
	old_pos[1] = e->cam->pos[1];
//	ft_log(LOG_DEBUG, 1, "Camera:");
//	ft_log(LOG_DEBUG, 0, "pos: (%f, %f)", e->cam->pos[0], e->cam->pos[1]);
//	ft_log(LOG_DEBUG, -1, "size: (%f, %f)", e->cam->size[0], e->cam->size[1]);

//	if (e->cam->input.moves || e->cam->status)
	update_uniforms(e);
}
