/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loop.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/15 17:06:04 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

int		loop(t_env *e)
{
	ft_log(LOG_FINE, 1, "Starting loop.");
	while (!glfwWindowShouldClose(e->window))
	{
		if (!events(e))
			return (ft_log(LOG_ERROR, -1, "Events error."));
		if (!update(e))
			return (ft_log(LOG_ERROR, -1, "update error."));
		if (!render(e))
			return (ft_log(LOG_ERROR, -1, "Render error."));
	}
	return (ft_log(LOG_FINE, -1, "End of loop."));
}
