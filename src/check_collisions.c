
#include "zatacka.h"

static float	*get_rectangle(float *r, float *p1, float *p2, float width)
{
	float	norm[2] = { p1[1] - p2[1], p2[0] - p1[0] };

	vec2_normalize(norm);
	r[0] = p1[0] - norm[0] * width / 2.0f;
	r[1] = p1[1] - norm[1] * width / 2.0f;
	r[2] = p2[0] - norm[0] * width / 2.0f;
	r[3] = p2[1] - norm[1] * width / 2.0f;
	r[4] = p2[0] + norm[0] * width / 2.0f;
	r[5] = p2[1] + norm[1] * width / 2.0f;
	r[6] = p1[0] + norm[0] * width / 2.0f;
	r[7] = p1[1] + norm[1] * width / 2.0f;
//	ft_log(LOG_DEBUG, 1, "Rectangle:");
//	ft_log(LOG_DEBUG, 0, "(%f, %f)", r[0], r[1]);
//	ft_log(LOG_DEBUG, 0, "(%f, %f)", r[2], r[3]);
//	ft_log(LOG_DEBUG, 0, "(%f, %f)", r[4], r[5]);
//	ft_log(LOG_DEBUG, -1, "(%f, %f)", r[6], r[7]);
	return (r);
}

static float	*get_arrow(float *a, float *p2, float *vec, float arrow_size, float speed)
{
	float	size[2] = { arrow_size * pow(speed, 0.5f), arrow_size / pow(speed, 0.3f) };
	float	axis_x[2];
	float	axis_y[2];
	float	origin[2];

	vec2_normalize(vec);
	axis_x[0] = vec[0] * size[0];
	axis_x[1] = vec[1] * size[0];
	axis_y[0] = -vec[1] * size[1];
	axis_y[1] = vec[0] * size[1];
	origin[0] = p2[0] + (arrow_size - size[0]) * axis_x[0] / size[0];
	origin[1] = p2[1] + (arrow_size - size[0]) * axis_x[1] / size[0];
	a[0] = origin[0] + 1.0f * axis_x[0] + 0.00f * axis_y[0];
	a[1] = origin[1] + 1.0f * axis_x[1] + 0.00f * axis_y[1];
	a[2] = origin[0] + -0.3f * axis_x[0] + -0.5f * axis_y[0];
	a[3] = origin[1] + -0.3f * axis_x[1] + -0.5f * axis_y[1];
	a[4] = origin[0] + -0.3f * axis_x[0] + 0.5f * axis_y[0];
	a[5] = origin[1] + -0.3f * axis_x[1] + 0.5f * axis_y[1];
//	ft_log(LOG_DEBUG, 0, "Origin: %f, %f", origin[0], origin[1]);
//	ft_log(LOG_DEBUG, 0, "Top: %f, %f", a[0], a[1]);
//	ft_log(LOG_DEBUG, 0, "Right: %f, %f", a[2], a[3]);
//	ft_log(LOG_DEBUG, 0, "Left: %f, %f", a[4], a[5]);
	return (a);
}

float			*check_collisions(float *p1, float *p2, 
						float *line_col, t_player **all_player, size_t num)
{
	size_t		i;
	size_t		j;
	//size_t		total;
	float		r1[8];
	float		r2[8];
	float		arrow[6];
	t_line		*l_indice;
	t_chunk		*curr_chunk;

	get_rectangle(r1, p1, p2, all_player[num]->size_line);
	get_arrow(arrow, p2, all_player[num]->dir, all_player[num]->size_arrow, all_player[num]->speed / all_player[num]->speed_min);
	//total = 0;
	j = 0;
	while (j < 4)
	{
		curr_chunk = all_player[num]->player_q_obj->q_chunk[j];
		if ((j <= 3 || all_player[num]->player_q_obj->q_chunk[3] != curr_chunk) && 
			(j <= 2 || all_player[num]->player_q_obj->q_chunk[2] != curr_chunk) && 
			(j <= 1 || all_player[num]->player_q_obj->q_chunk[1] != curr_chunk) && 
			(j <= 0 || all_player[num]->player_q_obj->q_chunk[0] != curr_chunk))
		{
			l_indice = (t_line *) curr_chunk->lines->data;
			//total += curr_chunk->lines->size;
			i = 0;
			while (i < curr_chunk->lines->size)
			{
				if (l_indice[i].player != num || ((all_player[num]->to_ignore + 2) < all_player[num]->last_line.index[1] && 
											l_indice[i].index[1] < ( all_player[num]->last_line.index[1] - (all_player[num]->to_ignore + 2))))
				{
					get_rectangle(r2, (float *) all_player[l_indice[i].player]->lines->data + 2 * (l_indice[i].index[0]),
									  (float *) all_player[l_indice[i].player]->lines->data + 2 * (l_indice[i].index[1]), all_player[l_indice[i].player]->size_line);
					if (collision_rectangle_polygon(r2, arrow, 3) || collision_rectangles(r1, r2))
					{
/*						norm_col[0] = - r2[3] + r2[1];
						norm_col[1] =   r2[2] - r2[0];
						vec2_normalize(norm_col);

						if (norm_col[0] * (r1[2] - r1[0]) +  norm_col[0] * (r1[3] - r1[1]) > 0)
						{
							norm_col[0] = - norm_col[0];
							norm_col[1] = - norm_col[1];
						}*/
						line_col[0] = r2[0];
						line_col[1] = r2[1];
						line_col[2] = r2[2];
						line_col[3] = r2[3];
						return (line_col);
					}
				}
				i++;
			}
		}
		j++;
	}

	//ft_log(LOG_DEBUG, 0, "player n°%d collide with %d lines ", num, total);
	bzero(line_col, sizeof(float) * 4);
	
	return (NULL);
}
