/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/02 10:57:58 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

#define FPS 60

static void		fix_fps(void)
{
//	static clock_t	t = 0;
	static time_t	t1 = 0;
	static size_t	frames = 0;

//	if (clock() - t < CLOCKS_PER_SEC / FPS)
//		usleep((CLOCKS_PER_SEC / FPS - (clock() - t)) * 1000);
//	t = clock();
	if (time(NULL) != t1)
	{
		ft_log(LOG_DEBUG, 0, "FPS: %zu", frames);
		frames = 0;
		t1 = time(NULL);
	}
	frames++;
}

/*static void		nb_lines(t_player *p)
{
	static size_t	nb = 0;
	static size_t	s_t = 0;
	static time_t	t = 0;

	if (time(NULL) != t)
	{
		if (s_t)
			ft_log(LOG_DEBUG, 0, "Lines: %zu, Average: %zu",
				p->lines->size / 4 - nb, nb / s_t);
		nb = p->lines->size / 4;
		t = time(NULL);
		s_t++;
	}
}
*/


static void		render_player(t_player *p)
{
	glBindVertexArray(p->vao_pos);
	glUseProgram(p->player_sp->shader_program.id);
	glUniform3f(p->player_sp->id_color, ((p->color & 0xFF0000) >> 16) / 255.0f,
											((p->color & 0x00FF00) >> 8) / 255.0f,
											(p->color & 0x0000FF) / 255.0f);
	glUniform1f(p->player_sp->id_speed, p->speed / p->speed_min);
	glUniform1i(p->player_sp->id_skin_arrow, p->skin_arrow);
	glUniform1i(p->player_sp->id_skin_back, p->skin_back);
	glUniform1f(p->player_sp->id_size, p->size_arrow);
	glDrawArrays(GL_POINTS, 0, 1);

}

static void		render_line(t_player *p)
{
	glBindVertexArray(p->vao_lines);
	glUseProgram(p->line_sp->shader_program.id);
	glUniform1f(p->line_sp->id_size, p->size_line);
	glUniform1i(p->line_sp->id_last_vert, ((GLuint*)p->indices->data)[p->indices->size - 1]);
	glUniform3f(p->line_sp->id_color, ((p->color & 0xFF0000) >> 16) / 255.0f,
											((p->color & 0x00FF00) >> 8) / 255.0f,
											(p->color & 0x0000FF) / 255.0f);
	glUniform1i(p->line_sp->id_skin_line, p->skin_line);
	glUniform1i(p->line_sp->id_skin_back, p->skin_back);
	glDrawElements(GL_TRIANGLES, p->indices->size, GL_UNSIGNED_INT, 0);
}

static void		render_background(t_background b, GLuint skin_b)
{
	glBindVertexArray(b.vao);
	glUseProgram(b.background_sp.shader_program.id);
	glUniform1i(b.background_sp.id_skin_back, skin_b);
	glDrawArrays(GL_POINTS, 0, b.nb_particles);
}

int		render(t_env *e)
{
	size_t		i;

	if (e->flags & F_DEBUG)
	{
		fix_fps();
//		nb_lines(e->player);
	}

	if (e->flags & F_ANTI_ALIASING)
		glBindFramebuffer(GL_FRAMEBUFFER, e->frame_buffer.msaa_buf);

	//glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	if (((t_player**)e->players->data)[0]->skin_back == 0)
	{
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	}
	else if (((t_player**)e->players->data)[0]->skin_back == 1)
	{
		glClearColor(0.07f, 0.07f, 0.1f, 1.0f);	
	}
	else
	{
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	}
	glClear(GL_COLOR_BUFFER_BIT);

	if (!(e->flags & F_NO_BACKGROUND))
		render_background(e->background,
				((t_player**)e->players->data)[0]->skin_back);

	i = 0;
	while (i < e->players->size)
	{
		render_line(((t_player**)e->players->data)[i]);
		i++;
	}
	i = 0;
	while (i < e->players->size)
	{
		render_player(((t_player**)e->players->data)[i]);
		i++;
	}

	if (e->flags & F_ANTI_ALIASING)
	{
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		glBindFramebuffer(GL_READ_FRAMEBUFFER, e->frame_buffer.msaa_buf);
		glDrawBuffer(GL_BACK);
		glBlitFramebuffer(0, 0, WIDTH, HEIGHT, 
			              0, 0, WIDTH, HEIGHT, 
			              GL_COLOR_BUFFER_BIT, GL_NEAREST);
	}

	//glFlush();
	glfwSwapBuffers(e->window);
	return (1);
}
