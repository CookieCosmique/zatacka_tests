/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/21 15:07:58 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/21 15:07:58 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

static int		run(int ac, char **av)
{
	t_env	*e;

	ft_log(LOG_FINE, 0, "Running \"zatacka.exe\"");
	if (!(e = init_env(ac, av)))
		return (ft_log(LOG_ERROR, 0, "Error while initializing environement."));
	if (!loop(e))
		return (ft_log(LOG_ERROR, 0, "Error in loop."));
	delete_env(&e);
	return (1);
}

int				main(int ac, char **av)
{
	if (!run(ac, av))
		return (ft_log(LOG_ERROR, 0, "Program ended.") + 1);
	return (ft_log(LOG_FINE, 0, "Program ended.") - 1);
}
