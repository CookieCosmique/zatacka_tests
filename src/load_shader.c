/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_shader.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 17:32:28 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/15 17:07:15 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

GLuint		load_shader(const char *path, GLenum shader_type)
{
	GLuint			shader;
	GLchar const	*source;
	GLint			status;
	char			buffer[512];

	if ((source = read_file(path)) == NULL)
		return (ft_log(LOG_ERROR, 0, "%s: %s", path, strerror(errno)));
	if ((shader = glCreateShader(shader_type)) == 0 || check_error())
		return (ft_log(LOG_ERROR, 0, "Can't create shader."));
	glShaderSource(shader, 1, &source, NULL);
	free((char*)source);
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		glGetShaderInfoLog(shader, 512, NULL, buffer);
		return (ft_log(LOG_ERROR, 0, "shader %s error : %s", path, buffer));
	}
	if (check_error())
		return (ft_log(LOG_ERROR, 0, "Can't load shader: %s", path));
	ft_log(LOG_FINE, 0, "Shader %s loaded.", path);
	return (shader);
}
