
#include "zatacka.h"

static float	projection_2d(float *p, float *v)
{
	return ((v[0] * p[0] + v[1] * p[1]) / sqrt((v[0] * v[0] + v[1] * v[1])));
}

static int		test_projection(float *d, float o1, float o2, float *r, size_t n)
{
	float		p;
	float		f;
	size_t		i;

	i = 0;
	f = projection_2d(r, d) - o1;
	while (i < n)
	{
		p = projection_2d(r + i, d);
		if ((p >= o1 && p <= o2) || (p <= o1 && p >= o2) || f * (p - o1) <= 0)
			return (1);
		i += 2;
	}
	return (0);
}

static int		projections_polygon(float *p1, size_t n1, float *p2, size_t n2)
{
	float	normale[2];
	size_t	i;

	i = 0;
	while (i < n1)
	{
		normale[0] = p1[(i + 1) % n1] - p1[(i + 3) % n1];
		normale[1] = p1[(i + 2) % n1] - p1[(i + 0) % n1];
		if (!(test_projection(normale, projection_2d(p1 + i, normale),
				projection_2d(p1 + (((i + 2) + 2 * (n1 / 4)) % n1),
				normale), p2, n2)))
			return (0);
		i += 2;
	}
	return (1);
}

static int		projections_rect_poly(float *r1, float *p, size_t nb)
{
	float		n1[2] = { r1[1] - r1[3], r1[2] - r1[0] };
	float		n2[2] = { r1[3] - r1[5], r1[4] - r1[2] };

	return (test_projection(n1, projection_2d(r1, n1), projection_2d(r1 + 4, n1), p, nb) &&
			test_projection(n2, projection_2d(r1 + 2, n2), projection_2d(r1, n2), p, nb));
}

int				collision_rectangles(float *r1, float *r2)
{
	return (projections_rect_poly(r1, r2, 8) &&
			projections_rect_poly(r2, r1, 8));
}

int				collision_rectangle_polygon(float *rect, float *p, size_t nb)
{
	return (projections_rect_poly(rect, p, nb * 2) &&
			projections_polygon(p, nb * 2, rect, 8));
}

int				collision_polygons(float *p1, size_t n1, float *p2, size_t n2)
{
	return (projections_polygon(p1, n1 * 2, p2, n2 * 2) &&
			projections_polygon(p2, n2 * 2, p1, n1 * 2));
}

int				collision_point_polygon(float *point, float *polygon, size_t nb)
{
	return (projections_polygon(polygon, nb * 2, point, 2));
}