/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_indice.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/15 17:06:04 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

t_alist		*add_indice(t_alist *indices, const GLuint value)
{
	return (alist_add(indices, &value));
}