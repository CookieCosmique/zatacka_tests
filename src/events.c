/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/02 10:10:44 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

static void		start_player_event(t_player *p, int event)
{
	p->input.moves |= (int)event;
}

static void		stop_player_event(t_player *p, int event)
{
	p->input.moves &= ~(int)event;
	if (p->draw && (event == PLAYER_ROTATION_LEFT || event == PLAYER_ROTATION_RIGHT))
		pop_a_point(p);
}

static int	events_player(GLFWwindow *window, t_player *p)
{
	size_t		i;
	int			event;

	i = 0;
	while (i < PLAYER_INPUT_NB)
	{
		event = (int)pow(2, i);
		if (!(p->input.moves & event) &&
				glfwGetKey(window, p->input.keys[i]) == GLFW_PRESS)
			start_player_event(p, event);
		if (p->input.moves & event &&
				glfwGetKey(window, p->input.keys[i]) == GLFW_RELEASE)
			stop_player_event(p, event);
		i++;
	}
	return (1);
}

static int	events_cam(t_env *e)
{
	size_t		i;

	i = 0;
	while (i < CAM_INPUT_NB)
	{
		if (glfwGetKey(e->window, e->cam->input.keys[i]) == GLFW_PRESS)
			e->cam->input.moves |= (int)pow(2, i);
		else if (glfwGetKey(e->window, e->cam->input.keys[i]) == GLFW_RELEASE)
			e->cam->input.moves &= ~(int)pow(2, i);
		i++;
	}
	return (1);
}

static int	events_game_speed(t_env *e)
{
	static int		tmp1 = 0;
	static int		tmp2 = 0;
	static int		tmp3 = 0;

	if (tmp1 == 0 && glfwGetKey(e->window, GLFW_KEY_L) == GLFW_PRESS)
	{
		e->game_speed /= 2.0f;
		tmp1 = 1;
	}
	if (tmp1 == 1 && glfwGetKey(e->window, GLFW_KEY_L) == GLFW_RELEASE)
		tmp1 = 0;
	if (tmp2 == 0 && glfwGetKey(e->window, GLFW_KEY_M) == GLFW_PRESS)
	{
		e->game_speed *= 2.0f;
		tmp2 = 1;
	}
	if (tmp2 == 1 && glfwGetKey(e->window, GLFW_KEY_M) == GLFW_RELEASE)
		tmp2 = 0;
	if (tmp3 == 0 && glfwGetKey(e->window, GLFW_KEY_P) == GLFW_PRESS)
	{
		e->pause = !e->pause;
		tmp3 = 1;
	}
	if (tmp3 == 1 && glfwGetKey(e->window, GLFW_KEY_P) == GLFW_RELEASE)
		tmp3 = 0;
	return (1);
}

int			events(t_env *e)
{
	size_t	i;

	glfwPollEvents();
	if (glfwGetKey(e->window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(e->window, GL_TRUE);
	events_game_speed(e);
	i = 0;
	while (i < e->players->size)
	{
		glfwPollEvents();
		events_player(e->window, ((t_player**)e->players->data)[i]);
		i++;
	}
	glfwPollEvents();
	events_cam(e);
	return (1);
}
