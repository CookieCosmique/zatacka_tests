/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_error.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 18:08:40 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/03 15:53:41 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

static const char	*get_error_str(GLuint error)
{
	int					i;
	static const GLuint	error_list[5] = {GL_NO_ERROR, GL_INVALID_ENUM,
		GL_INVALID_VALUE, GL_INVALID_OPERATION, GL_OUT_OF_MEMORY};
	static const char	*error_str[5] = {"GL_NO_ERROR", "GL_INVALID_ENUM",
		"GL_INVALID_VALUE", "GL_INVALID_OPERATION", "GL_OUT_OF_MEMORY"};

	i = 0;
	while (i < 5)
	{
		if (error_list[i] == error)
			return (error_str[i]);
		i++;
	}
	return (NULL);
}

unsigned int		check_error(void)
{
	GLuint		error;

	if ((error = glGetError()))
		ft_log(LOG_ERROR, 0, "%zu: %s", error, get_error_str(error));
	return (error);
}
