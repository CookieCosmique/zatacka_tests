/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/15 17:06:04 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vector.h"

float		*vec2(float const x, float const y)
{
	float	*vec;

	if (!(vec = (float*)malloc(sizeof(float) * 2)))
		return (NULL);
	vec[0] = x;
	vec[1] = y;
	return (vec);
}

float		vec2_norm(float *vec)
{
	return (sqrt(vec[0] * vec[0] + vec[1] * vec[1]));
}

float		*vec2_normalize(float *vec)
{
	float		norm;

	if (!(norm = vec2_norm(vec)))
	{
		vec[0] = 0;
		vec[1] = 0;
	}
	else
	{
		vec[0] = vec[0] / norm;
		vec[1] = vec[1] / norm;
	}
	return (vec);
}

float		*vec2_rotate(float *vec, const float angle)
{
	float	tmp_vec_0;

	tmp_vec_0 = vec[0];
	vec[0] = tmp_vec_0 * cos(angle) - vec[1] * sin(angle);
	vec[1] = tmp_vec_0 * sin(angle) + vec[1] * cos(angle);
	return (vec);
}