/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_shader_program.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 15:54:37 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/02 11:28:09 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

static int		load_shader_program(t_shader_program *sp,
		const char *vertex_shader, const char *geometry_shader,
		const char *fragment_shader)
{
	if (!(sp->vertex_shader = load_shader(vertex_shader,
					GL_VERTEX_SHADER)) || check_error())
		return (ft_log(LOG_ERROR, 0, "Shader error: %s", vertex_shader));
	if (!(sp->geometry_shader = load_shader(geometry_shader,
					GL_GEOMETRY_SHADER)) || check_error())
		return (ft_log(LOG_ERROR, 0, "Shader error: %s", geometry_shader));
	if (!(sp->fragment_shader = load_shader(fragment_shader,
					GL_FRAGMENT_SHADER)) || check_error())
		return (ft_log(LOG_ERROR, 0, "Shader error: %s", fragment_shader));
	sp->id = glCreateProgram();
	glAttachShader(sp->id, sp->vertex_shader);
	glAttachShader(sp->id, sp->geometry_shader);
	glAttachShader(sp->id, sp->fragment_shader);
	glBindFragDataLocation(sp->id, 0, "out_color");
	glLinkProgram(sp->id);
	glUseProgram(sp->id);
	if (check_error())
		return (ft_log(LOG_ERROR, 0, "Error while creating shader program."));
	return (ft_log(LOG_FINE, 0, "Shader program loaded"));
}

int				load_player_shader_program(t_player_sp *p)
{
	t_shader_program	*sp;

	ft_log(LOG_FINE, 1, "Loading player shader program..");
	sp = &(p->shader_program);
	if (!(load_shader_program(sp, "shaders/pvert.glsl", "shaders/pgeom.glsl",
			"shaders/pfrag.glsl")))
		return (ft_log(LOG_ERROR, -1, "Can't load shader program"));
	p->id_color = glGetUniformLocation(sp->id, "color");
	p->id_speed = glGetUniformLocation(sp->id, "speed");
	p->id_skin_arrow = glGetUniformLocation(sp->id, "skin_arrow");
	p->id_skin_back = glGetUniformLocation(sp->id, "skin_back");
	p->id_size = glGetUniformLocation(sp->id, "size");
	p->id_cam_size = glGetUniformLocation(sp->id, "cam_size");
	p->id_cam_pos = glGetUniformLocation(sp->id, "cam_pos");
	return (ft_log(LOG_FINE, -1, "Player shader program loaded."));
}

int				load_line_shader_program(t_line_sp *p)
{
	t_shader_program	*sp;

	ft_log(LOG_FINE, 1, "Loading line shader program..");
	sp = &(p->shader_program);
	if (!(load_shader_program(sp, "shaders/lvert.glsl", "shaders/lgeom.glsl",
			"shaders/lfrag.glsl")))
		return (ft_log(LOG_ERROR, -1, "Can't load shader program"));
	p->id_color = glGetUniformLocation(sp->id, "color");
	p->id_size = glGetUniformLocation(sp->id, "size");
	p->id_skin_line = glGetUniformLocation(sp->id, "skin_line");
	p->id_skin_back = glGetUniformLocation(sp->id, "skin_back");
	p->id_cam_size = glGetUniformLocation(sp->id, "cam_size");
	p->id_cam_pos = glGetUniformLocation(sp->id, "cam_pos");
	p->id_last_vert = glGetUniformLocation(sp->id, "last_vertex");
	return (ft_log(LOG_FINE, -1, "Line shader program loaded"));
}

int				load_background_shader_program(t_background_sp *p)
{
	t_shader_program	*sp;

	ft_log(LOG_FINE, 1, "Loading background shader program..");
	sp = &(p->shader_program);
	if (!(load_shader_program(sp, "shaders/bvert.glsl", "shaders/bgeom.glsl",
			"shaders/bfrag.glsl")))
		return (ft_log(LOG_ERROR, -1, "Can't load shader program"));
	p->id_skin_back = glGetUniformLocation(sp->id, "skin_back");
	p->id_cam_size = glGetUniformLocation(sp->id, "cam_size");
	p->id_cam_pos = glGetUniformLocation(sp->id, "cam_pos");
	p->id_map_size = glGetUniformLocation(sp->id, "map_size");
	p->id_player_pos = glGetUniformLocation(sp->id, "player_pos");
	p->id_player_center = glGetUniformLocation(sp->id, "player_center");
	p->id_nb_players = glGetUniformLocation(sp->id, "nb_players");
	p->id_particle_size = glGetUniformLocation(sp->id, "particle_size");

//	ft_log(LOG_FINE, 0, "uniform ? : play_pos : %d, play_cen : %d , play_nbr : %d, skin_back : %d, play_def : %d", p->id_play_pos, p->id_play_cen, p->id_play_nbr, p->id_skin_back, p->id_play_def);
	return (ft_log(LOG_FINE, -1, "Background shader program loaded."));
}
