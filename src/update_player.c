/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   update_player.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/02 10:10:46 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

#define SIZE_LINES 5.0f

static float	projection(float *p, float *v)
{
	return ((v[0] * p[0] + v[1] * p[1]) / sqrt((v[0] * v[0] + v[1] * v[1])));
}

static void		bounce(float *pos_2, float *pos, float *dir, float *line)
{
	float	norm[2];
	float	sca;
	float	o;

	norm[0] = -line[3] + line[1];
	norm[1] = line[2] - line[0];
	vec2_normalize(norm);
	o = projection(line, norm);
	if (fabsf(projection(pos_2, norm) - o) < fabsf(projection(pos, norm) - o))
	{
		sca = (dir[0] * norm[0] + dir[1] * norm[1]);
		dir[0] = dir[0] - 2 * sca * norm[0];
		dir[1] = dir[1] - 2 * sca * norm[1];
	}
}

static	float	*all_collision(float *pos, float *npos, t_alist *players,
			float *norm_col, size_t num)
{
//	size_t	i;
//	size_t 	to_ignore;

//	t_player	*player;
//	size_t		i;

	//to_ignore = 2 + ignore;
	/*i = 0;
	while (i < players->size)
	{
		to_ignore = 0;
		if (i == num)
			to_ignore = 2 + ignore;

		if (check_collisions(pos, npos, size_1,((t_player**)players->data)[i]->lines,
				((t_player**)players->data)[i]->indices, size_2, norm_col, to_ignore, speed))
		{
			return norm_col;
		}

		i++;
	}*/

	if (check_collisions(pos, npos, norm_col, (t_player **)players->data, num))
	{
		//ft_log(LOG_DEBUG, 0, "COLLISION");
		return (norm_col);
	}
	return (NULL);
}

static float	get_dist(float *p1, float *p2)
{
	return (sqrt((p1[0] - p2[0]) * (p1[0] - p2[0]) +
			(p1[1] - p2[1]) * (p1[1] - p2[1])));
}

static int		rotate_player(t_player *p, const float angle, const double t)
{
	float			dist;
	float			*last_point;

	last_point = ((float*)p->lines->data) + p->lines->size - 4;
	dist = get_dist(p->pos, last_point);
	p->dir = vec2_normalize(vec2_rotate(p->dir, angle * t));
	if (dist >= SIZE_LINES)
		return (1);
	return (0);
}


void			update_player(const size_t player_id, const double t,
		t_alist *players)
{
	float		new_pos[2];
	float		line_col[4];
	float		dep[2];
//	float		sca;
	int			pop;

	pop = 0;
	t_player *p = ((t_player**)players->data)[player_id];

	p->time_draw -= t;
	if (p->time_draw <= 0)
	{
		p->time_draw = 2.0f / (p->draw ? 10.0f : 1.0f);
		if (p->draw)
			stop_line(p);
		else
			start_line(p);
	}
	// Speed
	if (p->input.moves & 4)
		p->speed += (p->speed_max - p->speed) * 3.0f * t;
	else
		p->speed -= (p->speed - p->speed_min) * 3.0f * t;

	//Rotation
	if (p->input.moves & PLAYER_ROTATION_LEFT &&
			!(p->input.moves & PLAYER_ROTATION_RIGHT))
		pop |= rotate_player(p, p->rotation_speed, t);
	else if (p->input.moves & PLAYER_ROTATION_RIGHT &&
			!(p->input.moves & PLAYER_ROTATION_LEFT))
		pop |= rotate_player(p, -p->rotation_speed, t);

	// Position
	new_pos[0] = p->pos[0] + p->dir[0] * p->speed * t;
	new_pos[1] = p->pos[1] + p->dir[1] * p->speed * t;

	// Lines
//	if (p->draw)
//	{
//		((float*)p->lines->data)[p->back * 2 + 0] = p->pos[0];
//		((float*)p->lines->data)[p->back * 2 + 1] = p->pos[1];
//	}

	// Collision
	if (all_collision(p->pos, new_pos, players,
			line_col, player_id))
	{
		p->dead = 1;
		bounce(new_pos, p->pos, p->dir, line_col);
		new_pos[0] = p->pos[0] + p->dir[0] * p->speed * t;
		new_pos[1] = p->pos[1] + p->dir[1] * p->speed * t;
		pop = 1;
	}

	dep[0] = new_pos[0] - p->pos[0];
	dep[1] = new_pos[1] - p->pos[1];
	update_quad(dep, p->line_q_obj,  add_line_to_quad, &p->last_line);
	update_quad(dep, p->player_q_obj,  NULL, NULL);

	p->pos[0] = new_pos[0];
	p->pos[1] = new_pos[1];
	
	p->to_ignore -= p->to_ignore * t * 5.0f;
	if(p->to_ignore < 0)
		p->to_ignore = 0;

	if (p->draw && pop)
		pop_a_point(p);

	// VAO
	glBindVertexArray(p->vao_pos);
	glBindBuffer(GL_ARRAY_BUFFER, p->vbo_pos);
	glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float), p->pos, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, p->vbo_dir);
	glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float), p->dir, GL_DYNAMIC_DRAW);

	if (p->draw)
	{
		((float*)p->lines->data)[p->lines->size - 2] = p->pos[0];
		((float*)p->lines->data)[p->lines->size - 1] = p->pos[1];
	}

	glBindVertexArray(p->vao_lines);
	glBindBuffer(GL_ARRAY_BUFFER, p->vbo_lines);
	glBufferData(GL_ARRAY_BUFFER, p->lines->size * p->lines->content_size,
			p->lines->data, GL_DYNAMIC_DRAW);
}
