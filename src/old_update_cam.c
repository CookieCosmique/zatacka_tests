/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   update_cam.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/09 14:46:01 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

//TODO transition smooth using ratio
static float	update_ratio(float ratio, const float t, const float target)
{
	if (target >= ratio)
	{
		ratio += target * t;
		if (ratio >= target)
			return (target);
	}
	else
	{
		ratio -= (1.0f - target) * t;
		if (ratio <= target)
			return (target);
	}
	return (ratio);
}

static void		smooth(float *pos, float *from, float *target, const float t)
{
	pos[0] += (target[0] - from[0]) * t;
	pos[1] += (target[1] - from[1]) * t;
//	ft_log(LOG_DEBUG, 0, "%f = (%f - %f) * %f", pos[0], target[0], from[0], t);
}

static void		translate_cam(t_cam *cam, float t)
{
	if (cam->input.moves & CAM_MOVE_LEFT)
		cam->pos[0] -= cam->speed * cam->width / 1280.0f * t;
	if (cam->input.moves & CAM_MOVE_RIGHT)
		cam->pos[0] += cam->speed * cam->width / 1280.0f * t;
	if (cam->input.moves & CAM_MOVE_UP)
		cam->pos[1] += cam->speed * cam->height / 720.0f * t;
	if (cam->input.moves & CAM_MOVE_DOWN)
		cam->pos[1] -= cam->speed * cam->height / 720.0f * t;
	if (cam->input.moves & CAM_ZOOM_OUT)
	{
		cam->pos[0] -=  cam->width * (exp(t * cam->zoom_speed) - 1) / 2;
		cam->pos[1] -=  cam->height * (exp(t * cam->zoom_speed) - 1) / 2;
		cam->width *= exp(t * cam->zoom_speed);
		cam->height *= exp(t * cam->zoom_speed);
	}
	if (cam->input.moves & CAM_ZOOM_IN)
	{
		cam->pos[0] -=  cam->width * (exp(-t * cam->zoom_speed) - 1) / 2;
		cam->pos[1] -=  cam->height * (exp(-t * cam->zoom_speed) - 1) / 2;
		cam->width *= exp(-t * cam->zoom_speed);
		cam->height *= exp(-t * cam->zoom_speed);
	}
}

static void		update_follow_cam(t_follow_cam *cam, float *player_pos,
		const float width, const float height, float *pos)
{
	cam->width = width;
	cam->height = height;
	(void)pos;
//	cam->pos[0] = pos[0];
//	cam->pos[1] = pos[1];
	if (player_pos[0] < cam->pos[0] + cam->range * cam->width)
		cam->pos[0] = player_pos[0] - cam->width * cam->range;
	if (player_pos[0] > cam->pos[0] + (1 - cam->range) * cam->width)
		cam->pos[0] = player_pos[0] - cam->width * (1 - cam->range);
	if (player_pos[1] < cam->pos[1] + cam->range * cam->height)
		cam->pos[1] = player_pos[1] - cam->height * cam->range;
	if (player_pos[1] > cam->pos[1] + (1 - cam->range) * cam->height)
		cam->pos[1] = player_pos[1] - cam->height * (1 - cam->range);
}

//TODO move over time
static void		update_yolo_cam(t_yolo_cam *cam, t_player *p, const float t,
		const float width, const float height, float *pos)
{
	static float	player_pos_cam_s[2] = {0, 0};
	static float	player_dir_s[2] = {0, 0};
	float			player_pos_cam[2];
	float			range;

	cam->width = width;
	cam->height = height;
	(void)pos;
//	cam->pos[0] = pos[0];
//	cam->pos[1] = pos[1];
	range = cam->range / (((p->speed - p->speed_min) /
						(p->speed_max - p->speed_min)) + 1.0f);
	if (!player_dir_s[0] && !player_dir_s[1])
	{
		player_dir_s[0] = p->dir[0];
		player_dir_s[1] = p->dir[1];
	}
	else
		smooth(player_dir_s, player_dir_s, p->dir, t);
	player_pos_cam[0] = cam->width - (range * cam->width +
			(player_dir_s[0] + 1.0f) / 2.0f * ((1.0f - range * 2) *
				cam->width));
	player_pos_cam[1] = cam->height - (range * cam->height +
			(player_dir_s[1] + 1.0f) / 2.0f * ((1.0f - range * 2) *
				cam->height));
//	ft_log(LOG_DEBUG, 0, "player_pos_cam: %f, %f", player_pos_cam[0], player_pos_cam[1]);
	if (!player_pos_cam_s[0] && !player_pos_cam_s[1])
	{
		player_pos_cam_s[0] = player_pos_cam[0];
		player_pos_cam_s[1] = player_pos_cam[1];
	}
	else
		smooth(player_pos_cam_s, player_pos_cam_s, player_pos_cam, t);
//	ft_log(LOG_DEBUG, 0, "player_pos_cam_s: %f, %f", player_pos_cam_s[0], player_pos_cam_s[1]);
	cam->pos[0] = p->pos[0] - player_pos_cam_s[0];
	cam->pos[1] = p->pos[1] - player_pos_cam_s[1];
}

static void		update_uniforms(t_env *e)
{
	glUseProgram(e->player_sp.shader_program.id);
	glUniform2f(e->player_sp.id_cam_size, e->cam.width, e->cam.height);
	glUniform2fv(e->player_sp.id_cam_pos, 1, e->cam.pos);
	glUseProgram(e->line_sp.shader_program.id);
	glUniform2f(e->line_sp.id_cam_size, e->cam.width, e->cam.height);
	glUniform2fv(e->line_sp.id_cam_pos, 1, e->cam.pos);
	if (!(e->flags & F_NO_BACKGROUND))
	{
		glUseProgram(e->background.background_sp.shader_program.id);
		glUniform2f(e->background.background_sp.id_cam_size, e->cam.width,
				e->cam.height);
		glUniform2fv(e->background.background_sp.id_cam_pos, 1, e->cam.pos);
	}
}

static void		update_manual_cam(t_manual_cam *cam, const float width, const float height,
		const float *pos)
{
	cam->width = width;
	cam->height = height;
	(void)pos;
//	cam->pos[0] = pos[0];
//	cam->pos[1] = pos[1];
}

//TODO clean inputs
void			update_cam(t_env *e, const float t, const float game_speed)
{
	static int	tmp = 0;
	static int	tmp_2 = 0;
	static int	tmp_3 = 0;

	translate_cam(&e->cam, t);
	if (e->cam.input.moves & CAM_MANUAL && !tmp)
	{
		e->cam.manual_cam.pos[0] = e->cam.pos[0];
		e->cam.manual_cam.pos[1] = e->cam.pos[1];
		e->cam.status = 0;
		tmp = 1;
	}
	else if (!(e->cam.input.moves & CAM_MANUAL))
		tmp = 0;
	if (e->cam.input.moves & CAM_FOLLOW_P1 && !tmp_2)
	{
		e->cam.follow_cam.pos[0] = e->cam.pos[0];
		e->cam.follow_cam.pos[1] = e->cam.pos[1];
		e->cam.status = 1;
		tmp_2 = 1;
	}
	else if (!(e->cam.input.moves & CAM_FOLLOW_P1))
		tmp_2 = 0;
	if (e->cam.input.moves & CAM_FOLLOW_P1_2 && !tmp_3)
	{
		e->cam.yolo_cam.pos[0] = e->cam.pos[0];
		e->cam.yolo_cam.pos[1] = e->cam.pos[1];
		e->cam.status = 2;
		tmp_3 = 1;
	}
	else if (!(e->cam.input.moves & CAM_FOLLOW_P1_2))
		tmp_3 = 0;

	if (e->cam.manual_cam.ratio)
		update_manual_cam(&e->cam.manual_cam, e->cam.width, e->cam.height, e->cam.pos);
	if (e->cam.follow_cam.ratio)
		update_follow_cam(&e->cam.follow_cam,
				((t_player**)e->players->data)[0]->pos, e->cam.width,
				e->cam.height, e->cam.pos);
	if (e->cam.yolo_cam.ratio)
		update_yolo_cam(&e->cam.yolo_cam,
				((t_player**)e->players->data)[0], t * game_speed,
				e->cam.width, e->cam.height, e->cam.pos);

	e->cam.manual_cam.ratio = update_ratio(e->cam.manual_cam.ratio,
			t * game_speed, e->cam.status == 0 ? 1.0f : 0.0f);
	e->cam.follow_cam.ratio = update_ratio(e->cam.follow_cam.ratio,
			t * game_speed, e->cam.status == 1 ? 1.0f : 0.0f);
	e->cam.yolo_cam.ratio = update_ratio(e->cam.yolo_cam.ratio,
			t * game_speed, e->cam.status == 2 ? 1.0f : 0.0f);

	float	sum;

	sum = e->cam.manual_cam.ratio + e->cam.follow_cam.ratio + e->cam.yolo_cam.ratio;
	e->cam.pos[0] = (e->cam.manual_cam.pos[0] * e->cam.manual_cam.ratio / sum) +
					(e->cam.follow_cam.pos[0] * e->cam.follow_cam.ratio / sum) +
					(e->cam.yolo_cam.pos[0] * e->cam.yolo_cam.ratio / sum);

	e->cam.pos[1] = (e->cam.manual_cam.pos[1] * e->cam.manual_cam.ratio / sum) +
					(e->cam.follow_cam.pos[1] * e->cam.follow_cam.ratio / sum) +
					(e->cam.yolo_cam.pos[1] * e->cam.yolo_cam.ratio / sum);

/*	ft_log(LOG_DEBUG, 1, "Ratios: ");
	ft_log(LOG_DEBUG, 0, "Manual: %f", e->cam.manual_cam.ratio);
	ft_log(LOG_DEBUG, 0, "Follow: %f", e->cam.follow_cam.ratio);
	ft_log(LOG_DEBUG, -1, "Yolo: %f", e->cam.yolo_cam.ratio);
	ft_log(LOG_DEBUG, 1, "Positions: ");
	ft_log(LOG_DEBUG, 0, "Manual: %f, %f", e->cam.manual_cam.pos[0], e->cam.manual_cam.pos[1]);
	ft_log(LOG_DEBUG, 0, "Follow: %f, %f", e->cam.follow_cam.pos[0], e->cam.follow_cam.pos[1]);
	ft_log(LOG_DEBUG, 0, "Yolo: %f, %f",e->cam.yolo_cam.pos[0], e->cam.yolo_cam.pos[1]);
	ft_log(LOG_DEBUG, -1, "Cam: %f, %f",e->cam.pos[0], e->cam.pos[1]);*/

	if (e->cam.input.moves || e->cam.status)
		update_uniforms(e);
}
