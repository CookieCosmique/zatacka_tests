/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_cam.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/09 10:20:59 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/09 14:39:11 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

static int		set_inputs(t_cam *cam)
{
	cam->input.moves = 0;
	if (!(cam->input.keys = (int*)malloc(sizeof(int) * CAM_INPUT_NB)))
		return (ft_log(LOG_ERROR, 0, "Malloc error."));
	cam->input.keys[0] = GLFW_KEY_KP_4;
	cam->input.keys[1] = GLFW_KEY_KP_6;
	cam->input.keys[2] = GLFW_KEY_KP_8;
	cam->input.keys[3] = GLFW_KEY_KP_2;
	cam->input.keys[4] = GLFW_KEY_KP_ADD;
	cam->input.keys[5] = GLFW_KEY_KP_SUBTRACT;
	return (ft_log(LOG_FINE, 0, "Inputs initialized."));
}

static t_cam_type	*init_manual_cam(float *pos, float *size)
{
	t_cam_type	*cam;

	if (!(cam = (t_cam_type*)malloc(sizeof(t_cam_type))))
		return (ft_logp(LOG_ERROR, 0, NULL, "Malloc error."));
	cam->key = GLFW_KEY_1;
	cam->pressed = 0;
	cam->pos = vec2(pos[0], pos[1]);
	cam->size = vec2(size[0], size[1]);
	if (!cam->size || !cam->pos)
		return (ft_logp(LOG_ERROR, 0, NULL, "Malloc error."));
	cam->range = 0.0f;
	cam->ratio = 1.0f;
	cam->update = &update_manual_cam;
	return (ft_logp(LOG_FINE, 0, cam, "Manual cam initialized."));
}

static t_cam_type	*init_follow_cam(float *pos, float *size)
{
	t_cam_type	*cam;

	if (!(cam = (t_cam_type*)malloc(sizeof(t_cam_type))))
		return (ft_logp(LOG_ERROR, 0, NULL, "Malloc error."));
	cam->key = GLFW_KEY_2;
	cam->pressed = 0;
	cam->pos = vec2(pos[0], pos[1]);
	cam->size = vec2(size[0], size[1]);
	if (!cam->size || !cam->pos)
		return (ft_logp(LOG_ERROR, 0, NULL, "Malloc error."));
	cam->range = 0.3f;
	cam->ratio = 0.0f;
	cam->update = &update_follow_cam;
	return (ft_logp(LOG_FINE, 0, cam, "Follow cam initialized."));
}

static t_cam_type	*init_yolo_cam(float *pos, float *size)
{
	t_cam_type	*cam;

	if (!(cam = (t_cam_type*)malloc(sizeof(t_cam_type))))
		return (ft_logp(LOG_ERROR, 0, NULL, "Malloc error."));
	cam->key = GLFW_KEY_3;
	cam->pressed = 0;
	cam->pos = vec2(pos[0], pos[1]);
	cam->size = vec2(size[0], size[1]);
	if (!cam->size || !cam->pos)
		return (ft_logp(LOG_ERROR, 0, NULL, "Malloc error."));
	cam->range = 0.4f;
	cam->ratio = 0.0f;
	cam->update = &update_yolo_cam;
	return (ft_logp(LOG_FINE, 0, cam, "Follow cam initialized."));
}

int				init_cam(t_env *e, const float width, const float height)
{
	ft_log(LOG_FINE, 1, "Loading Camera");

	if (!(e->cam = (t_cam*)malloc(sizeof(t_cam))))
		return (ft_log(LOG_ERROR, -1, "Malloc error."));
	e->cam->size = vec2(width, height);
	e->cam->pos = vec2(0, 0);
	if (!e->cam->size || !e->cam->pos)
		return (ft_log(LOG_ERROR, -1, "Malloc error."));

	e->cam->status = 0;
	if (!(e->cam->cam_type = (t_cam_type**)malloc(sizeof(t_cam_type*) * CAM_TYPE_NB)))
		return (ft_log(LOG_ERROR, -1, "Malloc error."));
	e->cam->cam_type[0] = init_manual_cam(e->cam->pos, e->cam->size);
	e->cam->cam_type[1] = init_follow_cam(e->cam->pos, e->cam->size);
	e->cam->cam_type[2] = init_yolo_cam(e->cam->pos, e->cam->size);

	e->cam->trans_start_pos = (float *)malloc(sizeof(float) * CAM_TYPE_NB);
	e->cam->trans_curr_pos = (float *)malloc(sizeof(float) * CAM_TYPE_NB);
	e->cam->trans_dir = (float *)malloc(sizeof(float) * CAM_TYPE_NB);

	e->cam->trans_start_pos[0] = 1.0f;
	e->cam->trans_start_pos[1] = 0.0f;
	e->cam->trans_start_pos[2] = 0.0f;

	e->cam->trans_curr_pos[0] = 1.0f;
	e->cam->trans_curr_pos[1] = 0.0f;
	e->cam->trans_curr_pos[2] = 0.0f;

	e->cam->trans_dir[0] = 0.0f;
	e->cam->trans_dir[1] = 0.0f;
	e->cam->trans_dir[2] = 0.0f;

	e->cam->trans_duration = 0.0f;
	e->cam->trans_dir_size = 1.0f;

	if (!e->cam->cam_type[0] || !e->cam->cam_type[2] || !e->cam->cam_type[2])
		return (ft_log(LOG_ERROR, -1, "Malloc error."));

	if (!set_inputs(e->cam))
		return (ft_log(LOG_ERROR, -1, "Error while loading inputs."));
	e->cam->speed = 325.0f;
	e->cam->zoom_speed = 1.0f;

	glUseProgram(e->player_sp.shader_program.id);
	glUniform2fv(e->player_sp.id_cam_size, 1, e->cam->size);
	glUniform2fv(e->player_sp.id_cam_pos, 1, e->cam->pos);
	glUseProgram(e->line_sp.shader_program.id);
	glUniform2fv(e->line_sp.id_cam_size, 1, e->cam->size);
	glUniform2fv(e->line_sp.id_cam_pos, 1, e->cam->pos);
	if (!(e->flags & F_NO_BACKGROUND))
	{
		glUseProgram(e->background.background_sp.shader_program.id);
		glUniform2fv(e->background.background_sp.id_cam_size, 1, e->cam->size);
		glUniform2fv(e->background.background_sp.id_cam_pos, 1, e->cam->pos);
	}
	ft_log(LOG_FINE, 0, "width: %f", e->cam->size[0]);
	ft_log(LOG_FINE, 0, "height: %f", e->cam->size[1]);
	ft_log(LOG_FINE, 0, "pos: %f, %f", e->cam->pos[0], e->cam->pos[1]);
/*	ft_log(LOG_DEBUG, 1, "Camera %d:", 0);
	ft_log(LOG_DEBUG, 0, "pos: (%f, %f)", e->cam->cam_type[0].pos[0], e->cam->cam_type[0].pos[0]);
	ft_log(LOG_DEBUG, -1, "ratio: %f", e->cam->cam_type[0].ratio);
	ft_log(LOG_DEBUG, 1, "Camera %d:", 1);
	ft_log(LOG_DEBUG, 0, "pos: (%f, %f)", e->cam->cam_type[1].pos[0], e->cam->cam_type[1].pos[0]);
	ft_log(LOG_DEBUG, -1, "ratio: %f", e->cam->cam_type[1].ratio);
	ft_log(LOG_DEBUG, 1, "Camera %d:", 2);
	ft_log(LOG_DEBUG, 0, "pos: (%f, %f)", e->cam->cam_type[2].pos[0], e->cam->cam_type[2].pos[0]);
	ft_log(LOG_DEBUG, -1, "ratio: %f", e->cam->cam_type[2].ratio);*/

	return (ft_log(LOG_FINE, -1, "Camera loaded."));
}
