/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_file.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 16:37:58 by bperreon          #+#    #+#             */
/*   Updated: 2015/06/01 18:03:35 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

char	*read_file(const char *filename)
{
	char	*buffer;
	FILE	*file;
	int		length;

	if ((file = fopen(filename, "r")) == NULL)
		return (NULL);
	if (fseek(file, 0, SEEK_END) == -1)
		return (NULL);
	if ((length = ftell(file)) == -1)
		return (NULL);
	if (fseek(file, 0, SEEK_SET) == -1)
		return (NULL);
	if ((buffer = (char*)malloc(length + 1)) == NULL)
		return (NULL);
	fread(buffer, 1, length, file);
	fclose(file);
	buffer[length] = '\0';
	return (buffer);
}
