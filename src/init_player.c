/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_player.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/09 16:06:29 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

static int		init_lines(t_player *player, t_line_sp *line_sp,
		t_group_chunk *grp, size_t num)
{
	player->line_sp = line_sp;
	if (!(player->lines = alist_new(sizeof(float))))
		return (ft_log(LOG_ERROR, -1, "alist new error"));
	if (!(player->indices = alist_new(sizeof(GLuint))))
		return (ft_log(LOG_ERROR, -1, "alist new error"));

	glGenVertexArrays(1, &(player->vao_lines));
	glBindVertexArray(player->vao_lines);
	player->vbo_lines = load_vbo(line_sp->shader_program.id,
			player->lines->size * player->lines->content_size,
			player->lines->data, 1, "pos", 2, GL_DYNAMIC_DRAW);
	player->ebo_lines = load_ebo(player->indices->size *
			player->indices->content_size, player->indices->data);

	player->last_line.index[0] = 0;
	player->last_line.index[1] = 1;
	player->last_line.player =  num;
	player->line_q_obj = create_quad_obj(player->pos, player->size_arrow,
			player->size_arrow, grp, add_line_to_quad, &player->last_line);

	start_line(player);

	return (ft_log(LOG_FINE, 0, "Player lines loaded."));
}

static int		init_pos(t_player *player, t_player_sp *player_sp,
		t_group_chunk *grp)
{
	static double	x = 10;

	player->player_sp = player_sp;

	player->dir = vec2_normalize(vec2(0, 1));
	//player->pos = v
	//c2(x, 0.9f * HEIGHT);
	player->pos[0] = x;
	player->pos[1] = 10;
	x += 200.0f;
	//player->b_cen = vec2(HEIGHT / 2.0f, WIDTH / 2.0f);

	player->player_q_obj = create_quad_obj(player->pos, player->size_arrow,
			player->size_arrow, grp, NULL, NULL);
	ft_log(LOG_DEBUG, 0, "player pos : row : %d column : %d ",
			row_(player->player_q_obj->q_chunk[0], grp),
			col_(player->player_q_obj->q_chunk[0], grp));
	
	print_lines_in_chunk(grp->chunks + 1);
	print_lines_in_chunk(player->player_q_obj->q_chunk[0]);
	print_lines_in_chunk(player->player_q_obj->q_chunk[1]);
	print_lines_in_chunk(player->player_q_obj->q_chunk[2]);
	print_lines_in_chunk(player->player_q_obj->q_chunk[3]);

	glGenVertexArrays(1, &(player->vao_pos));
	glBindVertexArray(player->vao_pos);
	player->vbo_pos = load_vbo(player_sp->shader_program.id, 2 * sizeof(float),
		player->pos, 1, "pos", 2, GL_DYNAMIC_DRAW);
	player->vbo_dir = load_vbo(player_sp->shader_program.id, 2 * sizeof(float),
		player->dir, 2, "dir", 2, GL_DYNAMIC_DRAW);
	return (ft_log(LOG_FINE, 0, "Player pos init."));
}

static void		init_inputs(t_player *player)
{
	static int	keys[3][3] = {
		{ GLFW_KEY_LEFT, GLFW_KEY_RIGHT, GLFW_KEY_UP },
		{ GLFW_KEY_A, GLFW_KEY_D, GLFW_KEY_W },
		{ GLFW_KEY_H, GLFW_KEY_K, GLFW_KEY_U }
	};
	static int	nb = 0;

	if (nb < 3)
	{
		player->input.keys[0] = keys[nb][0];
		player->input.keys[1] = keys[nb][1];
		player->input.keys[2] = keys[nb][2];
	}
	player->input.moves = 0;
	nb++;
}

t_player		*init_player(const int color, const int skin,
		t_player_sp *player_sp, t_line_sp *line_sp, float * pos,
		t_group_chunk *grp, size_t num)
{
	t_player	*player;

	ft_log(LOG_FINE, 1, "Creating player..");
	if (!(player = (t_player*)malloc(sizeof(t_player))))
		return (ft_logp(LOG_ERROR, -1, NULL, "Malloc error"));

	player->dead = 0;
	player->time_draw = 2.0f;
	player->pos = pos;
	player->rotation_speed = M_PI * 2;
	player->speed = 150.0f;
	player->speed_min = player->speed;
	player->speed_max = player->speed * 3.5f;
	player->color = color;
	player->size_arrow = 18;
	player->size_line = 7;
	player->skin_line = skin;
	player->skin_arrow = skin;
	player->skin_back = skin;
	player->to_ignore = 1;

	if (!init_pos(player, player_sp, grp) ||
			!init_lines(player, line_sp, grp, num))
		return (ft_logp(LOG_ERROR, 0, NULL,
					"Error while initializing player position."));

	init_inputs(player);

	return (ft_logp(LOG_FINE, -1, player, "Player created."));
}
