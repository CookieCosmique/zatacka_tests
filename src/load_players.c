/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_players.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/09 15:28:10 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

static int		get_nb_players(int ac, char **av)
{
	int		nb;
	int		i;

	i = 0;
	nb = 0;
	while (++i < ac)
		if (av[i][0] >= '0' && av[i][0] <= '9')
			nb = atoi(av[i]);
	if (nb <= 0)
		return (1);
	return (nb);
}

int					load_players(t_env *e, int ac, char **av)
{
	size_t		nb;
	t_player	*player;
	size_t		i;
	float		*all_pos;

	nb = get_nb_players(ac, av);
	ft_log(LOG_FINE, 1, "Loading %zu players..", nb);
	if (!(e->players = alist_new(sizeof(t_player*))))
		return (ft_log(LOG_ERROR, -1, "alist new error."));
	if (!(load_player_shader_program(&(e->player_sp))))
		return (ft_log(LOG_ERROR, -1, "Can't load player shader program."));
	if (!(load_line_shader_program(&(e->line_sp))))
		return (ft_log(LOG_ERROR, -1, "Can't load line shader program."));
	i = 0;
	if (!(all_pos = (float*)malloc(2 * nb * sizeof(float))))
		return (ft_log(LOG_ERROR, -1, "Malloc error."));
	while (i < nb)
	{
		ft_log(LOG_FINE, 1, "Loading player %zu..", i);
		if (!(player = init_player(get_player_color(i + 1),
				(e->flags & F_NEON) ? 1 : 0, &(e->player_sp), &(e->line_sp),
				all_pos + 2 * i, e->group_chunk, i)))
			ft_log(LOG_ERROR, -1, "Can't init player");
		else
		{
			alist_add(e->players, &player);
			ft_log(LOG_FINE, -1, "Player %zu loaded.", i);
		}
		i++;
	}
	if (!e->players->size)
		return (ft_log(LOG_ERROR, -1, "0 players loaded."));
	return (ft_log(LOG_FINE, -1, "%zu players loaded.", e->players->size));
}
