/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   update.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/02 11:01:33 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

int				update(t_env *e)
{
	double	tmp;
	double	t;
	size_t	i;

	tmp = glfwGetTime();
	t = (tmp - e->t);
	e->t = tmp;
	i = 0;
	
	while (i < e->players->size)
	{
//		if (!(((t_player**)e->players->data)[i]->dead))
			update_player(i, t * e->game_speed * !e->pause, e->players);
		i++;
	}
	
	if (!(e->flags & F_NO_BACKGROUND))
		update_background(e->background, e->players, t * e->game_speed * !e->pause);
	update_cam(e, t, e->game_speed);
	return (1);
}
