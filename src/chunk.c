/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_shader_program.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 15:54:37 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/02 09:21:14 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

t_group_chunk	*create_chunk_group(float width, float height, float center_x, float center_y, int max_depth)
{	
	size_t			i;
	int				j;
	t_group_chunk 	*res;

	res = (t_group_chunk *) malloc(sizeof(t_group_chunk));
	res->width = width;
	res->height = height;
	res->chunk_w = res->width;
	res->chunk_h = res->height;

	res->center[0] = center_x;
	res->center[1] = center_y;
	res->up_left[0] = center_x - (res->width / 2.0f);
	res->up_left[1] = center_y + (res->height / 2.0f);
	res->max_depth = max_depth;
	res->size = 1 << max_depth;
	res->size_all = 1 << (2*max_depth);
	res->chunks = (t_chunk *) malloc(sizeof(t_chunk) * res->size_all);

	j = 0;
	while( j < max_depth)
	{
		res->chunk_w /= 2.0f;
		res->chunk_h /= 2.0f;

		j++;
	}

	i = 0;
	while ( i < res->size_all)
	{
		res->chunks[i].lines = alist_new(sizeof(t_line));
		i++;
	}

	return res;
}

t_chunk			*right_(t_chunk *curr, t_group_chunk *grp)
{
	if( (curr - grp->chunks) % grp->size < (grp->size - 1))
		return (curr + 1);
	else
		return grp->chunks + grp->size * ( (curr - grp->chunks) / grp->size ) + (grp->size - 1);
//		return NULL;
}

t_chunk			*right_n(t_chunk *curr, t_group_chunk *grp, size_t n)
{
	if( (curr - grp->chunks) % grp->size < (grp->size - n))
		return (curr + n);
	else
		return grp->chunks + grp->size * ( (curr - grp->chunks) / grp->size ) + (grp->size - 1);
//		return NULL;
}

t_chunk			*left_(t_chunk *curr, t_group_chunk *grp)
{
	if( (curr - grp->chunks) % grp->size > 0)
		return (curr - 1);
	else
		return grp->chunks + grp->size * (curr - grp->chunks) / grp->size;
//		return NULL;
}

t_chunk			*left_n(t_chunk *curr, t_group_chunk *grp, size_t n)
{
	if( (curr - grp->chunks) % grp->size > (n - 1) )
		return (curr - n);
	else
		return grp->chunks + grp->size * (curr - grp->chunks) / grp->size;
//		return NULL;
}

t_chunk			*up_(t_chunk *curr, t_group_chunk *grp)
{
	if( (size_t) (curr - grp->chunks) >= (grp->size) )
		return (curr - grp->size);
	else
		return grp->chunks;
//		return NULL;
}

t_chunk			*up_n(t_chunk *curr, t_group_chunk *grp, size_t n)
{
	if( (size_t) (curr - grp->chunks) >= (grp->size*n) )
		return (curr - n * grp->size);
	else
		return grp->chunks;
//		return NULL;
}

t_chunk			*down_(t_chunk *curr, t_group_chunk *grp)
{
	if( (size_t) (curr - grp->chunks) < (grp->size_all - grp->size) )
		return (curr + grp->size);
	else
		return grp->chunks + grp->size_all - 1;
//		return NULL;
}

t_chunk			*down_n(t_chunk *curr, t_group_chunk *grp, size_t n)
{
	if( (size_t) (curr - grp->chunks) < (grp->size_all - grp->size * n) )
		return (curr + n * grp->size);
	else
		return grp->chunks + grp->size_all - 1;
//		return NULL;
}

size_t			row_(t_chunk *curr, t_group_chunk *grp)
{
	if(curr == NULL)
		return -1;
	else
		return ( (size_t) (curr - grp->chunks) ) / grp->size;
}

size_t			col_(t_chunk *curr, t_group_chunk *grp)
{
	if(curr == NULL)
		return -1;
	else
		return ( (size_t) (curr - grp->chunks) ) % grp->size;
}

t_chunk 		*find_chunk_point(float *point, t_group_chunk *grp)
{
	int		curr_depth;
	size_t	curr_size;
	size_t	curr_size_all;
	float	curr_center[2];
	float	curr_scale[2];

	t_chunk *up_left_chunk;

	curr_center[0] = grp->center[0];
	curr_center[1] = grp->center[1];
	curr_scale[0] = grp->width / 2.0f;
	curr_scale[1] = grp->height / 2.0f;
	curr_size = grp->size;
	curr_size_all = grp->size_all;
	up_left_chunk = grp->chunks;
	
	curr_depth = 0;
	while (curr_depth < grp->max_depth)
	{
		curr_scale[0] = curr_scale[0] / 2.0f;
		curr_scale[1] = curr_scale[1] / 2.0f;
		
		curr_size = curr_size / 2;
		curr_size_all = curr_size_all / 2;

//		ft_log(LOG_DEBUG, 0, "new start ! ");

		if(point[0] < curr_center[0])
		{
			curr_center[0] -= curr_scale[0];
//			ft_log(LOG_DEBUG, 0, "going left ");
		}
		else
		{
			curr_center[0] += curr_scale[0];
			up_left_chunk += curr_size;
//			ft_log(LOG_DEBUG, 0, "going right ");
		}

		if(point[1] > curr_center[1])
		{
			curr_center[1] += curr_scale[0];
//			ft_log(LOG_DEBUG, 0, "going up ");
		}
		else
		{
			curr_center[1] -= curr_scale[1];
			up_left_chunk += curr_size_all;
//			ft_log(LOG_DEBUG, 0, "going down ");
		}

		curr_depth ++;
	}

	return (up_left_chunk);
}

t_pack_chunk 	*find_pack_chunk_poly(float *point, size_t nbr, t_group_chunk *grp)
{
	int				curr_depth;
	size_t			curr_size;
	size_t			curr_size_all;
	float			curr_center[2];
	float			curr_scale[2];
	t_pack_chunk	*res_pack;
	int				all_same;
	int				portion;
	int				tmp_portion;
	size_t			j;

	curr_center[0] = grp->center[0];
	curr_center[1] = grp->center[1];
	curr_scale[0] = grp->width / 2.0f;
	curr_scale[1] = grp->height / 2.0f;
	curr_size = grp->size;
	curr_size_all = grp->size_all;

	res_pack = (t_pack_chunk *) malloc(sizeof(t_pack_chunk));
	res_pack->ul_chunk = grp->chunks;
	res_pack->grp = grp;
	res_pack->depth = 0;
	curr_depth = 0;

	all_same = 1;
	while (curr_depth < grp->max_depth && all_same == 1)
	{
		curr_scale[0] = curr_scale[0] / 2.0f;
		curr_scale[1] = curr_scale[1] / 2.0f;
		
		curr_size = curr_size / 2;
		curr_size_all = curr_size_all / 2;

//		ft_log(LOG_DEBUG, 0, "new start ! ");
		all_same = -1;
		portion = -1;

		j = 0;
		while (j < nbr && all_same != 0)
		{
			tmp_portion = 0;
			if (point[2 * j + 0] < curr_center[0])
			{
				tmp_portion |= 1;
			}
			else
			{
				tmp_portion |= 2;
			}

			if (point[2 * j + 1] > curr_center[1])
			{
				tmp_portion |= 4;
			}
			else
			{
				tmp_portion |= 8;
			}

			if(all_same == -1)
			{
				portion = tmp_portion;
				all_same = 1;
			}
			else if(tmp_portion != portion)
				all_same = 0;

			ft_log(LOG_DEBUG, 0, "[point %d] tmp_portion ?  : %d : %d %d %d %d ", j, tmp_portion, (tmp_portion & 1) >> 0,  (tmp_portion & 2) >> 1, (tmp_portion & 4) >> 2, (tmp_portion & 8) >> 3);

			j++;
		}

		ft_log(LOG_DEBUG, 0, "[depth %d] all the same ?  : %d", curr_depth, all_same);

		if (all_same == 1)
		{
			if (portion & 1)
			{
				curr_center[0] -= curr_scale[0];
				ft_log(LOG_DEBUG, 0, "going left ");
			}
			else if (portion & 2)
			{
				curr_center[0] += curr_scale[0];
				res_pack->ul_chunk += curr_size;
				ft_log(LOG_DEBUG, 0, "going right ");
			}

			if (portion & 4)
			{
				curr_center[1] += curr_scale[0];
				ft_log(LOG_DEBUG, 0, "going up ");
			}
			else if (portion & 8)
			{
				curr_center[1] -= curr_scale[1];
				res_pack->ul_chunk += curr_size_all;
				ft_log(LOG_DEBUG, 0, "going down ");
			}

			curr_depth ++;
			res_pack->depth ++;
		}
	}

	return (res_pack);
}

float			*up_left_of_chunk(float *pos, t_chunk *curr, t_group_chunk *grp)
{
	pos[0] = grp->up_left[0] + col_(curr, grp) * grp->chunk_w;
	pos[1] = grp->up_left[1] - row_(curr, grp) * grp->chunk_h;

	return (pos);
}

t_chunk			*update_chunk_point(t_chunk *curr, float *new_point, t_group_chunk *grp)
{
	float	up_left[2];
	float	diff[2];
	t_chunk	*res_chunk = curr;

	up_left_of_chunk(up_left, curr, grp);
	diff[0] = new_point[0] - up_left[0];
	diff[1] = new_point[1] - up_left[1];

	if (diff[0] < 0)
	{
		res_chunk = left_n(res_chunk, grp, 1 + ((int) (- diff[0])) / grp->chunk_w);
	}
	else if (diff[0] > grp->chunk_w)
	{
		res_chunk = right_n(res_chunk, grp, ((int) (diff[0]))/ grp->chunk_w);
	}

	if (diff[1] < - grp->chunk_h)
	{
		res_chunk = down_n(res_chunk, grp,((int) (- diff[1])) / grp->chunk_h);
	}
	else if (diff[1] > 0)
	{
		res_chunk = up_n(res_chunk, grp,1 + ((int) (diff[1]) / grp->chunk_h));
	}

	return (res_chunk);
}

t_quad_obj		*create_quad_obj(float *pos_center, float width, float height, t_group_chunk *grp, void (*func) (t_chunk *chunk, void *data), void *data)
{
	t_quad_obj	*res;
	size_t		i;

	res = (t_quad_obj *) malloc(sizeof(t_quad_obj));

	res->q_pos[0] = pos_center[0] - width / 2.0f;
	res->q_pos[1] = pos_center[1] + height / 2.0f;
	res->q_pos[2] = pos_center[0] + width / 2.0f;
	res->q_pos[3] = pos_center[1] + height / 2.0f;
	res->q_pos[4] = pos_center[0] + width / 2.0f;
	res->q_pos[5] = pos_center[1] - height / 2.0f;
	res->q_pos[6] = pos_center[0] - width / 2.0f;
	res->q_pos[7] = pos_center[1] - height / 2.0f;

	res->grp = grp;

	i = 0;
	while( i < 4)
	{
		res->q_chunk[i] = update_chunk_point(grp->chunks, res->q_pos + 2*i, grp);
		//res->q_chunk[i] = find_chunk_point(res->q_pos + 2*i, grp);
		up_left_of_chunk(res->q_up_left + 2*i, res->q_chunk[i], grp);
		res->q_down_right[0 + 2*i] = res->q_up_left[0 + 2*i] + grp->chunk_w;
		res->q_down_right[1 + 2*i] = res->q_up_left[1 + 2*i] - grp->chunk_h;
		
		if (func &&
			(i <= 3 || res->q_chunk[3] != res->q_chunk[i]) && 
			(i <= 2 || res->q_chunk[2] != res->q_chunk[i]) && 
			(i <= 1 || res->q_chunk[1] != res->q_chunk[i]) && 
			(i <= 0 || res->q_chunk[0] != res->q_chunk[i])) 
			func(res->q_chunk[i], data);

		i++;
	}

	return res;
}

static int		is_new(t_chunk **buffer, t_chunk **curr_all, t_chunk *curr, size_t i)
{
	if ((i == 0 || curr_all[0] != curr) && 
		(i == 1 || curr_all[1] != curr) && 
		(i == 2 || curr_all[2] != curr) &&
		(i == 3 || curr_all[3] != curr) && 
	    buffer[0] != curr &&
	    buffer[1] != curr &&
	    buffer[2] != curr &&
	    buffer[3] != curr)
		return 1;
	else
		return 0;
}

static void		update_step_quad(t_quad_obj *q_obj, t_chunk **buffer, float *step, void (*func) (t_chunk *chunk, void *data), void *data)
{
	size_t i;



	buffer[0] = q_obj->q_chunk[0];
	buffer[1] = q_obj->q_chunk[1];
	buffer[2] = q_obj->q_chunk[2];
	buffer[3] = q_obj->q_chunk[3];

	i = 0;
	while (i < 4)
	{
		q_obj->q_pos[0 + 2*i] += step[0];

		if (q_obj->q_pos[0 + 2*i] > q_obj->q_down_right[0 + 2*i])
		{
			q_obj->q_down_right[0 + 2*i] += q_obj->grp->chunk_w;
			q_obj->q_up_left[0 + 2*i] += q_obj->grp->chunk_w;
			q_obj->q_chunk[i] = right_(q_obj->q_chunk[i], q_obj->grp);

			/*ft_log(LOG_FINE, 0, "i : %d ", i);
			ft_log(LOG_FINE, 0, "q_obj chunk[0] ? row : %d column : %d ", row_(q_obj->q_chunk[0], q_obj->grp), col_(q_obj->q_chunk[0], q_obj->grp));
			ft_log(LOG_FINE, 0, "q_obj chunk[1] ? row : %d column : %d ", row_(q_obj->q_chunk[1], q_obj->grp), col_(q_obj->q_chunk[1], q_obj->grp));
			ft_log(LOG_FINE, 0, "q_obj chunk[2] ? row : %d column : %d ", row_(q_obj->q_chunk[2], q_obj->grp), col_(q_obj->q_chunk[2], q_obj->grp));
			ft_log(LOG_FINE, 0, "q_obj chunk[3] ? row : %d column : %d ", row_(q_obj->q_chunk[3], q_obj->grp), col_(q_obj->q_chunk[3], q_obj->grp));

			ft_log(LOG_FINE, 0, "buffer[0] ? row : %d column : %d ", row_(buffer[0], q_obj->grp), col_(buffer[0], q_obj->grp));
			ft_log(LOG_FINE, 0, "buffer[1] ? row : %d column : %d ", row_(buffer[1], q_obj->grp), col_(buffer[1], q_obj->grp));
			ft_log(LOG_FINE, 0, "buffer[2] ? row : %d column : %d ", row_(buffer[2], q_obj->grp), col_(buffer[2], q_obj->grp));
			ft_log(LOG_FINE, 0, "buffer[3] ? row : %d column : %d ", row_(buffer[3], q_obj->grp), col_(buffer[3], q_obj->grp));*/


			if (func && is_new(buffer, q_obj->q_chunk, q_obj->q_chunk[i], i))
				func(q_obj->q_chunk[i], data);
		}
		else if (q_obj->q_pos[0 + 2*i] < q_obj->q_up_left[0 + 2*i])
		{
			q_obj->q_down_right[0 + 2*i] -= q_obj->grp->chunk_w;
			q_obj->q_up_left[0 + 2*i] -= q_obj->grp->chunk_w;
			q_obj->q_chunk[i] = left_(q_obj->q_chunk[i], q_obj->grp);

			/*ft_log(LOG_FINE, 0, "i : %d ", i);
			ft_log(LOG_FINE, 0, "q_obj chunk[0] ? row : %d column : %d ", row_(q_obj->q_chunk[0], q_obj->grp), col_(q_obj->q_chunk[0], q_obj->grp));
			ft_log(LOG_FINE, 0, "q_obj chunk[1] ? row : %d column : %d ", row_(q_obj->q_chunk[1], q_obj->grp), col_(q_obj->q_chunk[1], q_obj->grp));
			ft_log(LOG_FINE, 0, "q_obj chunk[2] ? row : %d column : %d ", row_(q_obj->q_chunk[2], q_obj->grp), col_(q_obj->q_chunk[2], q_obj->grp));
			ft_log(LOG_FINE, 0, "q_obj chunk[3] ? row : %d column : %d ", row_(q_obj->q_chunk[3], q_obj->grp), col_(q_obj->q_chunk[3], q_obj->grp));

			ft_log(LOG_FINE, 0, "buffer[0] ? row : %d column : %d ", row_(buffer[0], q_obj->grp), col_(buffer[0], q_obj->grp));
			ft_log(LOG_FINE, 0, "buffer[1] ? row : %d column : %d ", row_(buffer[1], q_obj->grp), col_(buffer[1], q_obj->grp));
			ft_log(LOG_FINE, 0, "buffer[2] ? row : %d column : %d ", row_(buffer[2], q_obj->grp), col_(buffer[2], q_obj->grp));
			ft_log(LOG_FINE, 0, "buffer[3] ? row : %d column : %d ", row_(buffer[3], q_obj->grp), col_(buffer[3], q_obj->grp));*/

			if (func && is_new(buffer, q_obj->q_chunk, q_obj->q_chunk[i], i))
				func(q_obj->q_chunk[i], data);
		}

		i++;
	}

	buffer[0] = q_obj->q_chunk[0];
	buffer[1] = q_obj->q_chunk[1];
	buffer[2] = q_obj->q_chunk[2];
	buffer[3] = q_obj->q_chunk[3];

	i = 0;
	while (i < 4)
	{

		q_obj->q_pos[1 + 2*i] += step[1];

		if (q_obj->q_pos[1 + 2*i] < q_obj->q_down_right[1 + 2*i])
		{
			q_obj->q_down_right[1 + 2*i] -= q_obj->grp->chunk_h;
			q_obj->q_up_left[1 + 2*i] -= q_obj->grp->chunk_h;
			q_obj->q_chunk[i] = down_(q_obj->q_chunk[i], q_obj->grp);

			/*ft_log(LOG_FINE, 0, "i : %d ", i);
			ft_log(LOG_FINE, 0, "q_obj chunk[0] ? row : %d column : %d ", row_(q_obj->q_chunk[0], q_obj->grp), col_(q_obj->q_chunk[0], q_obj->grp));
			ft_log(LOG_FINE, 0, "q_obj chunk[1] ? row : %d column : %d ", row_(q_obj->q_chunk[1], q_obj->grp), col_(q_obj->q_chunk[1], q_obj->grp));
			ft_log(LOG_FINE, 0, "q_obj chunk[2] ? row : %d column : %d ", row_(q_obj->q_chunk[2], q_obj->grp), col_(q_obj->q_chunk[2], q_obj->grp));
			ft_log(LOG_FINE, 0, "q_obj chunk[3] ? row : %d column : %d ", row_(q_obj->q_chunk[3], q_obj->grp), col_(q_obj->q_chunk[3], q_obj->grp));

			ft_log(LOG_FINE, 0, "buffer[0] ? row : %d column : %d ", row_(buffer[0], q_obj->grp), col_(buffer[0], q_obj->grp));
			ft_log(LOG_FINE, 0, "buffer[1] ? row : %d column : %d ", row_(buffer[1], q_obj->grp), col_(buffer[1], q_obj->grp));
			ft_log(LOG_FINE, 0, "buffer[2] ? row : %d column : %d ", row_(buffer[2], q_obj->grp), col_(buffer[2], q_obj->grp));
			ft_log(LOG_FINE, 0, "buffer[3] ? row : %d column : %d ", row_(buffer[3], q_obj->grp), col_(buffer[3], q_obj->grp));*/

			if (func && is_new(buffer, q_obj->q_chunk, q_obj->q_chunk[i], i))
				func(q_obj->q_chunk[i], data);
		}
		else if (q_obj->q_pos[1 + 2*i] > q_obj->q_up_left[1 + 2*i])
		{
			q_obj->q_down_right[1 + 2*i] += q_obj->grp->chunk_h;
			q_obj->q_up_left[1 + 2*i] += q_obj->grp->chunk_h;
			q_obj->q_chunk[i] = up_(q_obj->q_chunk[i], q_obj->grp);

			/*ft_log(LOG_FINE, 0, "i : %d ", i);
			ft_log(LOG_FINE, 0, "q_obj chunk[0] ? row : %d column : %d ", row_(q_obj->q_chunk[0], q_obj->grp), col_(q_obj->q_chunk[0], q_obj->grp));
			ft_log(LOG_FINE, 0, "q_obj chunk[1] ? row : %d column : %d ", row_(q_obj->q_chunk[1], q_obj->grp), col_(q_obj->q_chunk[1], q_obj->grp));
			ft_log(LOG_FINE, 0, "q_obj chunk[2] ? row : %d column : %d ", row_(q_obj->q_chunk[2], q_obj->grp), col_(q_obj->q_chunk[2], q_obj->grp));
			ft_log(LOG_FINE, 0, "q_obj chunk[3] ? row : %d column : %d ", row_(q_obj->q_chunk[3], q_obj->grp), col_(q_obj->q_chunk[3], q_obj->grp));

			ft_log(LOG_FINE, 0, "buffer[0] ? row : %d column : %d ", row_(buffer[0], q_obj->grp), col_(buffer[0], q_obj->grp));
			ft_log(LOG_FINE, 0, "buffer[1] ? row : %d column : %d ", row_(buffer[1], q_obj->grp), col_(buffer[1], q_obj->grp));
			ft_log(LOG_FINE, 0, "buffer[2] ? row : %d column : %d ", row_(buffer[2], q_obj->grp), col_(buffer[2], q_obj->grp));
			ft_log(LOG_FINE, 0, "buffer[3] ? row : %d column : %d ", row_(buffer[3], q_obj->grp), col_(buffer[3], q_obj->grp));*/

			if (func && is_new(buffer, q_obj->q_chunk, q_obj->q_chunk[i], i))
				func(q_obj->q_chunk[i], data);
		}

		i++;
	}
}

void			update_quad(float *dep, t_quad_obj *q_obj, void (*func) (t_chunk *chunk, void *data), void *data)
{	
	float	tmp_dep[2];
	float	step[2];
	int		typ;
	t_chunk *buffer[4];

	if (dep[0] == 0 && dep[1] == 0)
	{
		return;
	}
	else
	{
		tmp_dep[0] = 0;
		tmp_dep[1] = 0;

		if (fabsf(dep[0]) > fabsf(dep[1]))
		{
			if(dep[0] > 0)
				step[0] = q_obj->grp->chunk_w;
			else
				step[0] = - q_obj->grp->chunk_w;

			step[1] = step[0] * dep[1] / dep[0];
			typ = 0;
		}
		else
		{
			if(dep[1] > 0)
				step[1] = q_obj->grp->chunk_h;
			else
				step[1] = - q_obj->grp->chunk_h;

			step[0] = step[1] * dep[0] / dep[1];
			typ = 1;
		}

		while (dep[0] != tmp_dep[0] || dep[1] != tmp_dep[1])
		{
			//ft_log(LOG_DEBUG, 0, "NEW LOOP /////////");

			if ((typ == 0 && fabsf(tmp_dep[0]) + fabsf(step[0]) < fabsf(dep[0]) ) || (typ == 1 && fabsf(tmp_dep[1]) + fabsf(step[1]) < fabsf(dep[1])))
			{
				tmp_dep[0] += step[0];
				tmp_dep[1] += step[1];
			}
			else
			{
				step[0] = dep[0] - tmp_dep[0];
				step[1] = dep[1] - tmp_dep[1];
				tmp_dep[0] = dep[0];
				tmp_dep[1] = dep[1];
			}

			update_step_quad(q_obj, buffer, step, func, data);
		}
	}

}

void			recal_quad_on_curr_chunk(t_quad_obj *q_obj, void (*func) (t_chunk *chunk, void *data), void *data)
{
	size_t		i;

	i = 0;
	while( i < 4)
	{	
		if (func &&
			(i <= 3 || q_obj->q_chunk[3] != q_obj->q_chunk[i]) && 
			(i <= 2 || q_obj->q_chunk[2] != q_obj->q_chunk[i]) && 
			(i <= 1 || q_obj->q_chunk[1] != q_obj->q_chunk[i]) && 
			(i <= 0 || q_obj->q_chunk[0] != q_obj->q_chunk[i]))
			func(q_obj->q_chunk[i], data);

		i++;
	}
}

void			add_line_to_quad(t_chunk *chunk, void *data)
{
	alist_add(chunk->lines, data);
}

void			print_lines_in_chunk(t_chunk *chunk)
{
	if(chunk)
	{
		if(chunk->lines)
			ft_log(LOG_DEBUG, 0, "chunk : %d lines", chunk->lines->size);
		else
			ft_log(LOG_DEBUG, 0, "chunk[?] -> is NULL");
	}
}
