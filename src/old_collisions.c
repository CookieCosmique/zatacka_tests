/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   collisions.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/15 17:06:04 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

static float	projection_2d(float *p, float *v)
{
	return ((v[0] * p[0] + v[1] * p[1]) / sqrt((v[0] * v[0] + v[1] * v[1])));
}


static float	*min_max_proj(float *res, float *v, float *o, float *r, int nbr)
{
	float	v_tp[2];
	float	tmp;
	int		i;

	v_tp[0] = r[0] - o[0];
	v_tp[1] = r[1] - o[1];

	tmp = projection_2d(v_tp, v);
	res[0] = tmp;
	res[1] = tmp;

	i = 1;
	while( i < nbr )
	{
		v_tp[0] = r[0 + i * 2] - o[0];
		v_tp[1] = r[1 + i * 2] - o[1];

		tmp = projection_2d(v_tp, v);

		if(tmp < res[0])
			res[0] = tmp;

		if(tmp > res[1])
			res[1] = tmp;

		i++;
	}

	return res;
}


static int		my_stuff_projections(float *v, float *o,float *d, float *r2, int nbr)
{
	int		i;
	float	v_tp[2];
	float	p1;
	float	to_comp;
	float	opose;

	float	o1 = d[0];
	float	o2 = d[1];

	v_tp[0] = r2[0] - o[0];
	v_tp[1] = r2[1] - o[1];

	p1 =  projection_2d(v_tp, v);

	if( p1 < o1 )
	{
		to_comp = o1;
		opose = 1.0f;
	}
	else if (p1 <= o2)
	{
		return (1);
	}
	else
	{
		to_comp = o2;
		opose = -1.0f;
	}

	i = 1;
	while(i<nbr)	
	{	
		v_tp[0] = r2[0 + i * 2] - o[0];
		v_tp[1] = r2[1 + i * 2] - o[1];

		if( (to_comp - projection_2d(v_tp, v)) * opose < 0)
			return 1;

		i++;
	}

	return (0);
}


static int	just_some_proj_square_to_poly(float *r1 , float *r2, int nbr)
{
	float	v[2] = {r1[2] - r1[0], r1[3] - r1[1]};
	float	o[2] = {r1[0], r1[1]};
	float	d[2] = { 0, sqrt( (r1[2] - r1[0]) * (r1[2] - r1[0]) + (r1[3] - r1[1]) * (r1[3] - r1[1])) };

	return my_stuff_projections(v, o, d, r2, nbr);
}

static int	just_some_proj_poly_to_poly(float *v, float *r1, int nbr1, float *r2, int nbr2)
{
	float	o[2] = {r1[0], r1[1]};
	float	d[2];
	min_max_proj(d, v, o, r1, nbr1);
	return (my_stuff_projections(v, o, d, r2, nbr2));
}

static int		proj_square_to_poly(float *r1 , float *r2, int nbr)
{
	return (just_some_proj_square_to_poly(r1, r2, nbr) &&
			just_some_proj_square_to_poly(r1 + 2, r2, nbr));
}

static int		proj_square_to_square(float *r1 , float *r2)
{
	return (proj_square_to_poly(r1, r2, 4));
}

static int		proj_poly_to_poly(float *r1, int nbr1, float *r2, int nbr2)
{
	float	v[2];
	int		i;
	int		j;

	int		res;

	res = 1;
	i = 0;
	while (i < nbr1)
	{
		j = i + 1;
		if( j >= nbr1)
		{
			j = 0;
		}
		v[1] = -r1[j * 2 + 0] + r1[i * 2 + 0];
        v[0] = r1[j * 2 + 1] - r1[i * 2 + 1];
		res &= just_some_proj_poly_to_poly(v, r1, nbr1, r2, nbr2);
		i++;
	}
	//do stuff
	return res;
}

static int		collision_2d_rectangles(float *r1, float *r2)
{
	return (proj_square_to_square(r1, r2) &&
			proj_square_to_square(r2, r1));
}

static int		collision_2d_poly(float *r1, float *r2, int nbr)
{
	return (proj_square_to_poly(r1, r2, nbr) &&
			proj_poly_to_poly(r1, 4, r2, nbr));
}
/*
static float	*calc_center_rec(float *r, float *d1, float *v1, float len, float wid)
{
	float n_v[2];

	n_v[0] = v1[0]; 
	n_v[1] = v1[1];
	vec2_normalize(n_v);

	r[0] = d1[0] - n_v[1] * wid / 2.0;
	r[1] = d1[1] + n_v[0] * wid / 2.0;
	r[2] = r[0] + n_v[0] * len;
	r[3] = r[1] + n_v[1] * len;
	r[4] = r[2] + n_v[1] * wid;
	r[5] = r[3] - n_v[0] * wid;
	r[6] = r[4] - n_v[0] * len; 
	r[7] = r[5] - n_v[1] * len;

	return r;
}

static float	*get_rectangle(float *r, float *p1, float *p2, float size)
{
	float dir_v[2] = { (p2[0] - p1[0]), p2[1] - p1[1] }; // pas de rescale ( * ratio )
	float n_p[2] = {p1[0],p1[1]};						 // pas de rescale ( * ratio )
	float len = vec2_norm(dir_v);
	float wid = size;

	return (calc_center_rec(r, n_p, dir_v, len, wid));
}*/
/*
static float	*get_arrow(float *a, float *p1, float *p2, float arrow_size, float speed)
{
	float	vec[2] = { p2[0] - p1[0], p2[1] - p1[1] };
	float	size[2] = { arrow_size * pow(speed, 0.5f), arrow_size / pow(speed, 0.3f) };
	float	axis_x[2];
	float	axis_y[2];
	float	origin[2];

	vec2_normalize(vec);
	axis_x[0] = vec[0] * size[0];
	axis_x[1] = vec[1] * size[0];
	axis_y[0] = -vec[1] * size[1];
	axis_y[1] = vec[0] * size[1];
	origin[0] = p1[0];
	origin[1] = p1[1];
	a[0] = origin[0] + 0.999f * axis_x[0] + 0.00f * axis_y[0];
	a[1] = origin[1] + 0.999f * axis_x[1] + 0.00f * axis_y[1];
	a[2] = origin[0] + -0.0f * axis_x[0] + -0.3f * axis_y[0];
	a[3] = origin[1] + -0.0f * axis_x[1] + -0.3f * axis_y[1];
	a[4] = origin[0] + -0.0f * axis_x[0] + 0.3f * axis_y[0];
	a[5] = origin[1] + -0.0f * axis_x[1] + 0.3f * axis_y[1];
	return (a);
}
*/
//<<<<<<< HEAD
//float			*check_player_collisions_player(t_player *p1, float *new_pos, t_player *p2, float *norm_col);
/*float			*check_collisions(float *p1, float *p2, float p_size,
			t_alist *lines, t_alist *indices, float l_size, float *norm_col, size_t to_ignore, float speed)
=======
float			*check_player_collisions_player(t_player *p1, float *new_pos,
			t_player *p2, float *norm_col)
>>>>>>> 4caee31d90dafb66d28369d87a33c216b58d61f1
{
	size_t		i;
	float		r1[8];
	float		r2[8];
<<<<<<< HEAD
	float		arrow[6];
	float		*line;
	GLuint		*indice_;
	t_alist		*to_use_indices;

	to_use_indices = indices;
	line = ((float*)lines->data);
	indice_ = ((GLuint*)to_use_indices->data);
=======
	float		arrow[8];
	float		*lines;
	GLuint		*indices;
	size_t		ignore_lines;

	ignore_lines = p1 == p2 ? 2 + p1->ignore_lines : 0;
	if (p2->indices->size < ignore_lines * 3)
		return (NULL);
>>>>>>> 4caee31d90dafb66d28369d87a33c216b58d61f1

	lines = ((float*)p2->lines->data);
	indices = ((GLuint*)p2->indices->data);
	get_rectangle(r1, p1->pos, new_pos, p1->size_line);
	get_arrow(arrow, p1->pos, new_pos, p1->size_arrow, p1->speed / p1->speed_min);

	i = 0;
<<<<<<< HEAD
	
	while (i < (to_use_indices->size > (to_ignore * 3) ? to_use_indices->size - (to_ignore * 3) : 0))
	{
		get_rectangle(r2, line + (indice_[i + 1] * 2), line + (indice_[i + 2] * 2), l_size);
		if (*//*(to_use_indices->size > to_ignore * 1 * 3 && i < to_use_indices->size - (to_ignore * 1 * 3) &&
				*//*collision_2d_poly(r2, arrow, 3)*//*)*/ /*|| collision_2d_rectangles(r1, r2))
=======
	while (i < p2->indices->size - (ignore_lines * 3))
	{
		get_rectangle(r2, lines + indices[i + 1] * 2, lines + indices[i + 2] * 2, p2->size_line);
		if (collision_2d_poly(r2, arrow, 3) || collision_2d_rectangles(r1, r2))
>>>>>>> 4caee31d90dafb66d28369d87a33c216b58d61f1
		{
			norm_col[0] = - r2[3] + r2[1];
			norm_col[1] =   r2[2] - r2[0];
			vec2_normalize(norm_col);

			if(norm_col[0] * (r1[2] - r1[0]) +  norm_col[0] * (r1[3] - r1[1]) > 0)
			{
				norm_col[0] = - norm_col[0];
				norm_col[1] = - norm_col[1];
			}
			return (norm_col);
		}
		i += 3;
	}
<<<<<<< HEAD

	norm_col[0] = 0;
	norm_col[1] = 0;
	return (0);
}*/
/*
float			*check_collisions(float *p1, float *p2, 
						float *norm_col, t_player **all_player, size_t num)
{
	size_t		i;
	size_t		j;
	//size_t		total;
	float		r1[8];
	float		r2[8];
	float		arrow[6];
	t_line		*l_indice;
	t_chunk		*curr_chunk;

	get_rectangle(r1, p1, p2, all_player[num]->size_line);
	get_arrow(arrow, p1, p2, all_player[num]->size_arrow, all_player[num]->speed / all_player[num]->speed_min);
	//total = 0;
	j = 0;
	while (j < 4)
	{
		curr_chunk = all_player[num]->player_q_obj->q_chunk[j];
		if ((j <= 3 || all_player[num]->player_q_obj->q_chunk[3] != curr_chunk) && 
			(j <= 2 || all_player[num]->player_q_obj->q_chunk[2] != curr_chunk) && 
			(j <= 1 || all_player[num]->player_q_obj->q_chunk[1] != curr_chunk) && 
			(j <= 0 || all_player[num]->player_q_obj->q_chunk[0] != curr_chunk))
		{
			l_indice = (t_line *) curr_chunk->lines->data;
			//total += curr_chunk->lines->size;
			i = 0;
			while (i < curr_chunk->lines->size)
			{
				if (l_indice[i].player != num || ((all_player[num]->to_ignore + 2) < all_player[num]->last_line.index[1] && 
											l_indice[i].index[1] < ( all_player[num]->last_line.index[1] - (all_player[num]->to_ignore + 2))))
				{
					get_rectangle(r2, (float *) all_player[l_indice[i].player]->lines->data + 2 * (l_indice[i].index[0]),
									  (float *) all_player[l_indice[i].player]->lines->data + 2 * (l_indice[i].index[1]), all_player[l_indice[i].player]->size_line);
					if (collision_2d_poly(r2, arrow, 3) || collision_2d_rectangles(r1, r2))
					{
						norm_col[0] = - r2[3] + r2[1];
						norm_col[1] =   r2[2] - r2[0];
						vec2_normalize(norm_col);

						if(norm_col[0] * (r1[2] - r1[0]) +  norm_col[0] * (r1[3] - r1[1]) > 0)
						{
							norm_col[0] = - norm_col[0];
							norm_col[1] = - norm_col[1];
						}
						return (norm_col);
					}
				}
				i ++;
			}
		}
		j++;
	}

	//ft_log(LOG_DEBUG, 0, "player n°%d collide with %d lines ", num, total);
	norm_col[0] = 0;
	norm_col[1] = 0;
	
	return (NULL);
}

void			test_collisions()
{
	float d1[2] = {0.0,0.0};
	float v1_1[2] = {1.0,0.0};
	float len_1 = 1.0f;
	float wid_1 = 0.01f;

	float d2[2] = {0.5,0.05};
	float v2_1[2] = {0.0,-1.0};
	float len_2 = 0.2f;
	float wid_2 = 0.01f;

	float r1[8];
	float r2[8];

	calc_center_rec(r1, d1, v1_1, len_1, wid_1);
	calc_center_rec(r2, d2, v2_1, len_2, wid_2);

	ft_log(LOG_DEBUG,0, "TESTING STUFF");
	ft_log(LOG_DEBUG,0, "collision ? %d",collision_2d_rectangles(r1, r2));

	ft_log(LOG_DEBUG,0, "END STUFF");

}*/