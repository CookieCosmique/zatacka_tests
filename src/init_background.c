/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_background.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/02 11:43:07 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

static float	*get_particle_pos(const size_t nb_x, const size_t nb_y,
		const size_t dist)
{
	float		*pos;
	size_t		i;
	size_t		j;

	if (!(pos = (float*)malloc(sizeof(float) * nb_x * nb_y * 2)))
		return (ft_logp(LOG_ERROR, 0, NULL, "Malloc error."));
	i = 0;
	while (i < nb_y)
	{
		j = 0;
		while (j < nb_x)
		{
			pos[i * nb_x * 2 + j * 2] = j * dist - (nb_x * dist / 2.0f);
			pos[i * nb_x * 2 + j * 2 + 1] = i * dist - (nb_y * dist / 2.0f);
			j++;
		}
		i++;
	}
	return (ft_logp(LOG_FINE,0, pos, "Particle pos created."));
}
static int		load_background_vao(t_background *b)
{
	glGenVertexArrays(1, &(b->vao));
	glBindVertexArray(b->vao);
	b->vbo_particle_pos = load_vbo(b->background_sp.shader_program.id,
			2 * sizeof(float) * b->nb_particles, b->particle_pos, 1, "particle_pos", 2,
			GL_STATIC_DRAW);
	return (!check_error());
}

int				init_background(t_background *b, const size_t nb_players,
		t_map map)
{
	size_t		nb_x;
	size_t		nb_y;

	nb_x = map.width / 128;
	nb_y = map.height / 128;
	b->nb_particles = nb_x * nb_y;
	b->particle_size = 128;
	ft_log(LOG_FINE, 1, "Creating background..");
	if (!(b->players_pos = (float*)malloc(sizeof(float) * nb_players * 2)))
		return (ft_log(LOG_ERROR, -1, "Malloc error."));
	if (!(b->players_center = (float*)malloc(sizeof(float) * nb_players * 2)))
		return (ft_log(LOG_ERROR, -1, "Malloc error."));
	bzero(b->players_center, sizeof(float) * nb_players * 2);
	if (!(b->particle_pos = get_particle_pos(nb_x, nb_y, b->particle_size)))
		return (ft_log(LOG_ERROR, -1, "Error while creating particle pos."));
	if (!load_background_shader_program(&(b->background_sp)))
		return (ft_log(LOG_ERROR, -1, "Shaders error."));
	if (!load_background_vao(b))
		return (ft_log(LOG_ERROR, -1, "Error while loading vao/"));
	glUniform2f(b->background_sp.id_map_size, map.width, map.height);
	return (ft_log(LOG_FINE, -1, "Background created."));
}
