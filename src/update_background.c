/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   update_background.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/02 10:54:27 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

void		update_background(t_background b, t_alist *players, const double t)
{
	size_t		i;
	t_player	*p;

	i = 0;
	while (i < players->size)
	{
		p = ((t_player**)players->data)[i];
		b.players_pos[i * 2] = p->pos[0];
		b.players_pos[i * 2 + 1] = p->pos[1];
		b.players_center[i * 2] += (p->dir[0] * p->speed * t) *
			pow(p->speed / p->speed_min, 2);
		b.players_center[i * 2 + 1] += (p->dir[1] * p->speed * t) *
			pow(p->speed / p->speed_min, 2);
		i++;
	}
	glUseProgram(b.background_sp.shader_program.id);
	glUniform1ui(b.background_sp.id_nb_players, players->size);
	glUniform2fv(b.background_sp.id_player_pos, players->size, b.players_pos);
	glUniform2fv(b.background_sp.id_player_center, players->size, b.players_center);
	glUniform1ui(b.background_sp.id_particle_size, b.particle_size);
	glUniform1ui(b.background_sp.id_nb_players, players->size);
}
