/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_env.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/09 10:30:09 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

#define 	MAP_W 16384
#define 	MAP_H 16384
#define		CHUNK_DEPTH 6
#define		CAM_WIDTH 1280
#define		CAM_HEIGHT 720

static int				prepare_glew(void)
{
	# if defined(_WIN32) || defined(__CYGWIN__)
		ft_log(LOG_FINE, 0, "Glew initialization");
		GLenum err = glewInit();
		if (err != GLEW_OK)
			return (ft_log(LOG_ERROR, 0, "Couldnt initialize glew: %s",
				glewGetErrorString(err)));
		return (ft_log(LOG_FINE, 0, "Glew initialized."));
	# endif
	return (1);
}

static GLFWwindow		*create_window(int width, int height, const char *title,
		GLFWmonitor *monitor, GLFWwindow *share)
{
	GLFWwindow	*window;
	GLint		major;
	GLint		minor;

	major = 4;
	while (major > 0)
	{
		minor = 9;
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, major);
		while (minor > 0)
		{
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor);
			if ((window = glfwCreateWindow(width, height, title, monitor, share)))
				return (window);
			minor--;
		}
		major--;
	}
	return (NULL);
}

static GLFWwindow		*init_window(void)
{
	GLFWwindow			*window;
	const GLFWvidmode	*mode;
	GLint				major;
	GLint				minor;

	if (!glfwInit())
		return (ft_logp(LOG_ERROR, 0, NULL, "Can't initialize glfw."));
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_DECORATED, GL_FALSE);
//	glfwWindowHint(GLFW_SAMPLES, 16);
	if (FULLSCREEN)
		window = create_window(WIDTH, HEIGHT, "Zatacka", glfwGetPrimaryMonitor(),
				NULL);
	else
		window = create_window(WIDTH, HEIGHT, "Zatacka", NULL, NULL);
	if (!window)
		return (ft_logp(LOG_ERROR, 0, NULL, "Window not created."));
	mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	glfwSetWindowPos(window, (mode->width - WIDTH) / 2, (mode->height - HEIGHT) / 2);
	glfwMakeContextCurrent(window);
	//glfwSwapInterval(1);
	glGetIntegerv(GL_MAJOR_VERSION, &major);
	glGetIntegerv(GL_MINOR_VERSION, &minor);
	ft_log(LOG_FINE, 0, "Using Opengl %d.%d", major, minor);
	ft_log(LOG_FINE, 0, "Gl version: %s", glGetString(GL_VERSION));
	return (ft_logp(LOG_FINE, 0, window, "Window initialized"));
}

static t_frame_buffer	init_frame_buffer(void)
{
	t_frame_buffer	frame_buffer;

	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &frame_buffer.msaa_tex);
	glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, frame_buffer.msaa_tex);
	glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, MSAA, GL_RGBA8, 
		                    WIDTH, HEIGHT, 0);

	glGenFramebuffers(1, &frame_buffer.msaa_buf);
	glBindFramebuffer(GL_FRAMEBUFFER, frame_buffer.msaa_buf);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 
						   GL_TEXTURE_2D_MULTISAMPLE, frame_buffer.msaa_tex, 0);

	glGenRenderbuffers(1, &frame_buffer.rbo_depth);
	glBindRenderbuffer(GL_RENDERBUFFER, frame_buffer.rbo_depth);
	glRenderbufferStorageMultisample(GL_RENDERBUFFER, MSAA,
			GL_DEPTH_COMPONENT32F, WIDTH, HEIGHT);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, 
		                      GL_RENDERBUFFER, frame_buffer.rbo_depth);
	check_error();
	return (frame_buffer);
}

static void				init_map(t_env *e)
{
	e->map.width = MAP_W;
	e->map.height = MAP_H;
}

void					to_call_test(t_chunk *chunk, void *data)
{
	ft_log(LOG_FINE, 0, "NEW CHUNK : row : %d column : %d ", row_(chunk, (t_group_chunk *) data), col_(chunk,(t_group_chunk *) data));
}

static void				test_on_chunks(t_env *e)
{
	t_pack_chunk *pack;
	float	test_poly[8] = {200,200,200,550,550,550,550,200};

	pack = find_pack_chunk_poly(test_poly, 4, e->group_chunk);

	ft_log(LOG_FINE, 0, "up left chunk : (row : %d column : %d) ; depth : %d", row_(pack->ul_chunk, pack->grp), col_(pack->ul_chunk, pack->grp), pack->depth);
}

static void				init_chunks(t_env *e)
{
	ft_log(LOG_FINE, 0, "size of map ? : %d, %d ", (int) e->map.width, (int) e->map.height);
	e->group_chunk = create_chunk_group(e->map.width, e->map.height,e->map.width/2, e->map.height/2, CHUNK_DEPTH);
	test_on_chunks(e);
}

t_env					*init_env(int ac, char **av)
{
	t_env	*e;

	ft_log(LOG_FINE, 1, "Initializing environement..");
	if (!(e = (t_env*)malloc(sizeof(t_env))))
		return (ft_logp(LOG_ERROR, -1, NULL, "Malloc error."));
	e->flags = get_flags(ac, av);
	if (e->flags & F_DEBUG)
		ft_log_add_flags(F_LOG_DEBUG);
	if (!(e->window = init_window()))
		return (ft_logp(LOG_ERROR, -1, NULL, "Error while loading window."));
	if (!prepare_glew())
		return (ft_logp(LOG_ERROR, -1, NULL, "Error while initializing glew."));

	init_map(e);
	init_chunks(e);

	if (!load_players(e, ac, av))
		return (ft_logp(LOG_ERROR, -1, NULL, "Error while creating player."));
	if (!(e->flags & F_NO_BACKGROUND) && !(init_background(&e->background,
					e->players->size, e->map)))
		return (ft_logp(LOG_ERROR, -1, NULL, "Error while creating background."));
	if (e->flags & F_ANTI_ALIASING)
		e->frame_buffer = init_frame_buffer();
	e->t = glfwGetTime();
	e->game_speed = 1.0f;
	e->pause = 0;

	if (!init_cam(e, CAM_WIDTH, CAM_HEIGHT))
		return (ft_logp(LOG_ERROR, -1, NULL, "Error while creating camera."));

	return (ft_logp(LOG_FINE, -1, e, "Environement initalized."));

}
