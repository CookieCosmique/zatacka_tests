/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pop_a_point.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/15 17:06:04 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

static void	pop_point(t_player *p, GLuint back, GLuint backer)
{
	alist_add(p->lines, &(p->pos[0]));
	alist_add(p->lines, &(p->pos[1]));

	add_indice(p->indices, backer);
	add_indice(p->indices, back);
	add_indice(p->indices, (GLuint)(p->lines->size / 2) - 1);

	p->last_line.index[0] = ((GLuint*)p->indices->data)[p->indices->size - 2];
	p->last_line.index[1] = ((GLuint*)p->indices->data)[p->indices->size - 1];

	recal_quad_on_curr_chunk(p->line_q_obj, add_line_to_quad, &p->last_line);

	glBindVertexArray(p->vao_lines);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, p->ebo_lines);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, p->indices->size *
			p->indices->content_size, p->indices->data, GL_DYNAMIC_DRAW);

	p->to_ignore++;
}

void		start_line(t_player *p)
{
	alist_add(p->lines, &(p->pos[0]));
	alist_add(p->lines, &(p->pos[1]));
	pop_point(p, (GLuint)(p->lines->size / 2 - 1),
			(GLuint)(p->lines->size / 2 - 1));
	p->draw = 1;
}

void		stop_line(t_player *p)
{
	pop_point(p, ((GLuint*)p->indices->data)[p->indices->size - 1],
			((GLuint*)p->indices->data)[p->indices->size - 2]);
	p->draw = 0;
}

void 		pop_a_point(t_player *p)
{
	pop_point(p, ((GLuint*)p->indices->data)[p->indices->size - 1],
			((GLuint*)p->indices->data)[p->indices->size - 2]);
}
