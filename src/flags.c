/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/21 15:07:58 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/21 15:07:58 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

static void		print_unknown_flag(t_flag *flags_list, const char c,
		const char *path)
{
	char	str[NB_FLAGS + 1];
	size_t	i;

	i = 0;
	while (i < NB_FLAGS)
	{
		str[i] = flags_list[i].c;
		i++;
	}
	str[i] = '\0';
	ft_log(LOG_WARNING, 0, "Unknown flag -\"%c\"", c);
	ft_log(LOG_WARNING, 0, "Usage: %s [-%s] [NB]", path, str);
}

static void		add_flags(const char *s, int *flags, const char *path)
{
	static t_flag	flags_list[NB_FLAGS] = {
		{F_DEBUG, 'd'},
		{F_ANTI_ALIASING, 'a'},
		{F_NO_BACKGROUND, 'b'},
		{F_NEON, 'n'},
		{F_BACKGROUND_DEFORME, 'f'}
	};
	size_t			i;
	size_t			j;

	if (s[0] != '-')
		return ;
	j = 0;
	while (s[++j] != '\0')
	{
		i = 0;
		while (i < NB_FLAGS)
		{
			if (s[j] == flags_list[i].c)
			{
				*flags |= flags_list[i].flag;
				break ;
			}
			i++;
		}
		if (i == NB_FLAGS)
			print_unknown_flag(flags_list, s[j], path);
	}
}

int				get_flags(int ac, char **av)
{
	int		flags;
	size_t	i;

	flags = 0;
	i = 0;
	while (++i < (size_t)ac)
		add_flags(av[i], &flags, av[0]);
	return (flags);
}