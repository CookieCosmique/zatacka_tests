/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loaders.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/16 12:45:44 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/06 19:02:16 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "zatacka.h"

GLuint			load_ebo(GLsizeiptr size, const GLvoid *data)
{
	GLuint	ebo;

	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, GL_DYNAMIC_DRAW);
	check_error();
	return (ebo);
}

GLuint			load_vbo(const GLuint program_id, GLsizeiptr size,
		const GLvoid *data, const GLuint index, const char *name,
		GLint nb, GLenum usage)
{
	GLuint	vbo;

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, size, data, usage);
	glBindAttribLocation(program_id, index, name);
	glEnableVertexAttribArray(index);
	glVertexAttribPointer(index, nb, GL_FLOAT, GL_FALSE, 0, NULL);
	check_error();
	return (vbo);
}

GLuint			load_vbo_tf(GLsizeiptr size, const GLvoid *data)
{
	GLuint	vbo;

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_DYNAMIC_DRAW);
	check_error();
	return (vbo);
}
