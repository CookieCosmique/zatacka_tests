#version 400 core

layout(location = 1) in vec2	pos;
layout(location = 2) in vec2	dir;

out vec2	out_dir;

void	main(void)
{
	out_dir = dir;
	gl_Position = vec4(pos, 0.0f, 1.0f);
}