#version 400 core

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

//in vec2 	vout_cent[];

out vec2	out_uv;
//out vec2	out_play_pos;
//out vec2	out_cent;

uniform vec2	cam_size;
uniform vec2	cam_pos;

void	emit_point(vec2 pos)
{
	out_uv = vec2((pos.x + 1.0) * cam_size.x/2 +  cam_pos.x, (pos.y + 1.0)  * cam_size.y/2 + cam_pos.y);
	gl_Position = vec4(pos.x, pos.y, 0f, 1f);
	EmitVertex();
}


void	main(void)
{
	float	resize = 1.0f;
	//out_play_pos = vec2(gl_in[0].gl_Position.x, gl_in[0].gl_Position.y);
	//out_cent = vec2(vout_cent[0].x, vout_cent[0].y);
	emit_point(vec2(-resize, -resize));
	emit_point(vec2( resize, -resize));
	emit_point(vec2(-resize,  resize));
	emit_point(vec2( resize,  resize));
	EndPrimitive();
}