#version 400 core

layout(location = 1) in vec2	pos;

out int	in_num;

void	main(void)
{
	in_num = gl_VertexID;
	gl_Position = vec4(pos, 0.0f, 1.0f);
}
