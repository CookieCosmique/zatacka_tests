#version 400 core

in vec2		pos;
in float	out_size;

out vec4	out_color;

uniform int		skin_back;

void	main(void)
{
	float	dist;
	float	band;
	float	max;

	vec4	color1;
	vec4	color2;
	vec4	col_med;
	vec4	col_o;

	dist = length(pos);

	if (skin_back == 0)
	{
		color1 = vec4(0.0f, 0.50f, 0.50f, 1.0f);
		color2 = vec4(0.0f, 0.0f, 0.0f, 1.0f);
	}
	else if (skin_back == 1)
	{
		color1 = vec4(0.21f, 0.2f, 0.2f, 1.0f);
		color2 = vec4(0.07f, 0.07f, 0.1f, 1.0f);
	}
	else
	{
		color1 = vec4(1.0f, 0.0f, 0.0f, 1.0f);
		color2 = vec4(0.0f, 0.0f, 0.0f, 1.0f);
	}

	col_med = (out_size) * color1 + (1 - out_size) * color2;

	max = 0.8;
	band = 0.03f;


	band /= out_size;

	if (dist <= max)
	{
		col_o = col_med;
		//discard;
	}
	else if (dist <= (max + band))
	{
		col_o = ((max + band - dist) / band) * col_med + (1 -  ((max + band - dist) / band)) * color2;
	}
	else
	{
		col_o = color2;
		discard;
	}

	out_color = col_o;
}
