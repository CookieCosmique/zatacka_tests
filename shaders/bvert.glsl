#version 400 core

layout(location = 1) in vec2	particle_pos;

void	main(void)
{
	gl_Position = vec4(particle_pos, 0.0f, 1.0f);
}
