#version 400 core

in	vec2	out_uv;
//in	vec2	out_play_pos;
//in 	vec2	out_cent;

out vec4	out_color;

uniform int		skin_back;
uniform int		play_nbr;
uniform int		play_def;

uniform vec2	play_pos[50];
//uniform vec2	play_cen[100];

float	moded(float a, float b)
{
	if (b == 0)
		return (0);
	return (a - (b * int(a / b)));
}

float	closest_mode(float a, float b)
{
	if (b == 0)
		return (0);
	if(a < 0)
		return -(b * int((-a + (b / 2.0f)) / b));
	else
		return (b * int((a + (b / 2.0f)) / b));
}

vec2 	nearest_center(vec2 pos, vec2 center, float size)
{
	vec2	tmp;

	tmp = pos - center;
	tmp.x = closest_mode(tmp.x, size);
	tmp.y = closest_mode(tmp.y, size);
	tmp += center;
	return tmp;
}

vec2	mod_center(vec2 cen, float size)
{
	cen.x = cen.x - closest_mode(cen.x, size);
	/*if (cen.x > size)
	{
		cen.x = 2*size - cen.x;
	}
	else if (cen.x < - size)
	{
		cen.x = - 2*size - cen.x;
	}*/
	cen.y = cen.y - closest_mode(cen.y, size);
	/*if (cen.y > size)
	{
		cen.y = 2*size - cen.y;
	}
	else if (cen.y < - size)
	{
		cen.y = - 2*size - cen.y;
	}*/
	return cen;
}

void	main(void)
{
	
	vec2 	u_p;
	vec2	p_p;
	vec2	center_all;

	float	size_all;	//taille d'un carre atour du cercle , donc le diametre
	float	ratio_follow;
	vec2	circle_center;
	float	circle_size;
	float	max_size;
	float	max_size_ratio;
	float	max_ratio;
	float	max_dist;
	float	coeff;
	float	power;
	float	min_size;
	float	dist_act;
	float	etend;

	float	p1;
	float	p2;
	float	ld;
	float	a;
	float	b;

	float	res;

	vec4	col_1;
	vec4	col_2;
	vec4	col_med;
	vec4	col_o;

	float	band;


	vec2	real_cent;
	vec2	real_circle_center;
	float	sum_inv_length;
	float 	percent_use[100];
	int 	player_real[100];
	float	treshold;
	float	per_play_inf;
	float	total_res;
	int i;
	int real_nbr = play_nbr;
	//int closest = 0;
	
	treshold = 0.01f;
	u_p = out_uv;
	//p_p = out_play_pos;

	per_play_inf = 100;
	i = 0;
	sum_inv_length = 0;
	while( i < play_nbr )
	{
		//percent_use[i] = 1/pow(length(u_p - play_pos[i]),2);
		percent_use[i] = exp(-length(u_p - play_pos[i])/per_play_inf);
		sum_inv_length += percent_use[i];
		player_real[i] = i;
//		if(percent_use[closest] < percent_use[i])
//		{
//			closest = i;
//		}
		i++;
	}

	i = 0;
	while( i < real_nbr )
	{
		if(percent_use[player_real[i]] / sum_inv_length < treshold)
		{
			sum_inv_length -= percent_use[player_real[i]];
			player_real[i] = (real_nbr - 1);
			real_nbr --;
		}
		else
		{
			//player_real[player_real[i]] = player_real[i];
			i++;
		}
	}

	if (skin_back == 0)
	{
		col_1 = vec4(0.0f, 0.0f, 0.0f, 1.0f);
		col_2 = vec4(0.0f, 0.25f, 0.25f, 1.0f);
		ratio_follow = -50f;
		min_size = 0.05f;
	}
	else if (skin_back == 1)
	{
		col_1 = vec4(0.07f, 0.07f, 0.1f, 1.0f);
		col_2 = vec4(0.21f, 0.2f, 0.2f, 1.0f);
		ratio_follow = -30f;
		min_size = 0.0f;
	}
	else
	{
		col_1 = vec4(1.0f, 0.0f, 0.0f, 1.0f);
		col_2 = vec4(0.0f, 0.0f, 0.0f, 1.0f);
		ratio_follow = -30f;
		min_size = 0.0f;
	}

	//col_1 = (vec4(0.0,1.0,0.0,1.0) * percent_use[0] + vec4(1.0,0.0,0.0,1.0) * percent_use[1] + vec4(0.0,0.0,1.0,1.0) * percent_use[2]) / sum_inv_length;

	ratio_follow = -0.5f;


	band = 0.05f;

	power = 0.9f;
	etend = 400f;

	size_all = 120f;

	max_size_ratio = 0.45f;
	max_dist = 200f;

	p1 = 2.0f;
	p2 = 1.0f;
	ld = 0.2f;

	max_size = size_all * max_size_ratio;
	min_size = min_size * size_all;
	band = band * max_size;

	ld = ld * max_size;
	a = (max_size - ld) / pow(max_dist, p1);
	b = (max_size - a * pow(max_dist, p1)) * pow(max_dist, p2);

	real_cent = vec2(0.0,0.0);
	//real_circle_center = vec2(0.0,0.0);

	if( play_def != 0)
	{
		i = 0;
		while( i < real_nbr)
		{
			//real_cent += play_cen[i] * percent_use[i] / sum_inv_length;
			real_cent += play_pos[player_real[i]] * percent_use[player_real[i]] / sum_inv_length;
			//real_cent += play_pos[i];
			//real_circle_center += nearest_center(u_p, play_cen[i] * ratio_follow, size_all) * percent_use[i] / sum_inv_length;
			//real_circle_center += mod_center(play_cen[i] * ratio_follow, size_all) * percent_use[i] / sum_inv_length;
			//real_circle_center += play_cen[i] * (ratio_follow * percent_use[i] / sum_inv_length);
			//real_circle_center += play_cen[i] * (ratio_follow * percent_use[i] / sum_inv_length);
			i++;
		}
	}

	//center_all = ratio_follow * p_p; // 1 player old func
	//center_all = out_cent * ratio_follow;  
	center_all = real_cent * ratio_follow;
	coeff = max_size / (pow(max_dist, power));
	circle_center = nearest_center(u_p, center_all, size_all); // 1 player old func
	//circle_center = nearest_center(u_p, vec2(0.0,0.0), size_all); // 1 player old func
	//circle_center = real_circle_center;
	//circle_center = nearest_center(u_p, real_circle_center, size_all);
	//circle_center = nearest_center(u_p, real_cent, size_all);
	i = 0;
	//dist_act = length(p_p - circle_center); // 1 player old func
	dist_act = 0;
	while( i < real_nbr)
	{
		dist_act += length(play_pos[player_real[i]] - circle_center) * percent_use[player_real[i]] / sum_inv_length;
		i++;
	}
	//circle_size = max(min_size,max_size - coeff*pow(dist_act,power));
	//circle_size = max(min_size,max_size*exp(-(pow(dist_act,power)/pow(etend,power))));
	if(dist_act <= max_dist)
	{
		circle_size = max(min_size, max_size - a * pow(dist_act, p1));
	}
	else
	{
		circle_size = max(min_size, b / pow(dist_act, p2));
	}

	//circle_size = size_all*0.4;
	res = length(u_p - circle_center) + band * 1.01;

	col_med = (circle_size / max_size) * col_2 + (1 - (circle_size / max_size)) * col_1;
	//col_med = col_2;
	
	if (res <= circle_size)
	{
		col_o = col_med;
		//discard;
	}
	else if (res <= (circle_size + band))
	{
		col_o = (((circle_size + band) - res) / band) * col_med + (1 -  (((circle_size + band) - res) / band)) * col_1;
	}
	else
	{
		col_o = col_1;
	}

	if(real_nbr <= 0)
	{
		col_o = col_1;
	}

	/*i = 0;
	total_res = 0;
	col_o = vec4(0.0,0.0,0.0,0.0);
	while (i < play_nbr)
	{
		circle_center = nearest_center(u_p, play_cen[i] * ratio_follow, size_all);
		dist_act = length(play_pos[i] - circle_center);

		if(dist_act <= max_dist)
		{
			circle_size = max(min_size, max_size - a * pow(dist_act, p1));
		}
		else
		{
			circle_size = max(min_size, b / pow(dist_act, p2));
		}

		res = length(u_p - circle_center) + band * 1.01;

		col_med = (circle_size / max_size) * col_2 + (1 - (circle_size / max_size)) * col_1;

		if (res <= circle_size)
		{
			col_o += col_med * percent_use[i] / sum_inv_length;
			//discard;
		}
		else if (res <= (circle_size + band))
		{
			col_o += ((((circle_size + band) - res) / band) * col_med + (1 -  (((circle_size + band) - res) / band)) * col_1) * percent_use[i] / sum_inv_length;
		}
		else
		{
			col_o += col_1 * percent_use[i] / sum_inv_length;
		}

		i++;
	}*/


	/*i = 0;
	col_o = 0;
	while ( i < play_nbr )
	{
		real_circle_center =  play_cen[i] * ratio_follow;
		col_o += / sum_inv_length;
	}*/


	/*if(play_nbr > 10 || length(play_pos[0] - play_cen[0]) > 0)
	{
		col_o = vec4(0.0,1.0,0.0,1.0);
	}*/


	/*if (length(p_p - u_p) > 50)
	{
		col_o = vec4(1.0,0.0,1.0,1.0);
	}
	else
	{
		col_o = vec4(0.0,0.0,1.0,1.0);
	}*/

	/*if (length(real_cent * ratio_follow - u_p) < 2000)
	{
		col_o = vec4(1.0,0.0,1.0,1.0) * ( 1 - length(real_cent * ratio_follow - u_p) / 2000);
	}*/

	//col_o = (vec4(0.0,1.0,0.0,1.0) * percent_use[0] + vec4(1.0,0.0,0.0,1.0) * percent_use[1] + vec4(0.0,0.0,1.0,1.0) * percent_use[2]) / sum_inv_length;
	
	/*if ( closest == 0 )
	{
		col_o = vec4(1.0,0.0,1.0,1.0);
	}
	if ( closest == 1 )
	{
		col_o = vec4(1.0,0.0,0.0,1.0);
	}
	else if ( closest == 2 )
	{
		col_o = vec4(0.0,1.0,0.0,1.0);
	}*/

	/*if (length(circle_center-u_p) < size_all * 0.2)
	{
		//col_o = vec4(1.0,1.0,1.0,1.0)* ( 1 - ((length(circle_center-u_p) / (size_all * 0.2))));
		col_o = vec4(0.0,0.0,0.0,1.0);
	}*/
	/*else
	{
		col_o = vec4(0.0,0.0,0.0,1.0);
	}*/
	/*else
	{
		col_o = vec4(0.0,0.0,1.0,1.0);
	}*/

	/*if(play_nbr > 1000)
	{
		col_o = vec4(play_cen[0].x,0.0,0.0,0.0);
	}*/

	out_color = col_o;
	//out_color = vec4(1.0,0.0,1.0,1.0);
}