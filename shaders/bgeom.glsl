#version 400 core

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

uniform vec2	cam_size;
uniform vec2	cam_pos;

uniform vec2	map_size;

uniform uint	nb_players;
uniform vec2	player_pos[50];
uniform vec2	player_center[50];
uniform uint	particle_size;

uniform int		skin_back;

out vec2	pos;
out float	out_size;

void	emit_point(vec2 pos)
{
	gl_Position = vec4((pos.x - cam_pos.x) / cam_size.x * 2 - 1.0f,
						(pos.y - cam_pos.y) / cam_size.y * 2 - 1.0f, 0.0f, 1.0f);
	EmitVertex();
}

float	moded(float a, float b)
{
	if (b == 0)
		return (0);
	return ((a - (b * int(a / b))) + (a < 0 ? b : 0));
}

float	get_dist(vec2 p1, vec2 p2)
{
	vec2	tmp;

	tmp.x = p2.x - p1.x;
	tmp.y = p2.y - p1.y;

	tmp.x = moded(tmp.x, map_size.x);
	tmp.y = moded(tmp.y, map_size.y);

	tmp.x = map_size.x/2.0f - abs(map_size.x/2.0f - tmp.x);
	tmp.y = map_size.y/2.0f - abs(map_size.y/2.0f - tmp.y);

	return (length(tmp));
}

vec2	get_closest_player(vec2 particle_pos)
{
	uint	id;
	float	closest;
	float	tmp;
	uint	i;

	closest = get_dist(particle_pos, player_pos[0]);
	id = 0;
	i = 0;
	while (++i < nb_players && i < 50)
	{
		if ((tmp = get_dist(particle_pos, player_pos[i])) < closest)
		{
			closest = tmp;
			id = i;
		}
	}
	return (player_pos[id]);
}

vec2	mod_center(vec2	cent)
{
	vec2	tmp;

	tmp = cent;
	//tmp = vec2(0.0f,0.0f);

	/*tmp.x = moded(cent.x, 20 * particle_size);
	tmp.y = moded(cent.y, 20 * particle_size);*/

	return tmp;
}

float	repart_func(float a, float renorm)
{
	return exp(-a/renorm);
}

vec2	get_particle_pos(vec2 particle_pos)
{
	vec2		final_pos;
	uint		i;
	float		max_dist;
	float		current_dist;
	float		player_percent[50];
	float		etend;
	float 		player_influence = 2000;
	float		ratio_follow = -0.05f;
	if (nb_players == 1)
		final_pos = particle_pos + ratio_follow * mod_center(player_center[0]);
	else
	{
		max_dist = 0;
		//etend = 1500.0f;

		//etend = repart_func(etend, player_influence);
		i = 0;
		while (i < nb_players && i < 50)
		{
			current_dist = repart_func(get_dist(particle_pos, player_pos[i]),player_influence);
			/*if (current_dist > etend)
				current_dist = -1;
			else*/
			max_dist += current_dist;
			player_percent[i] = current_dist;
			i++;
		}
		final_pos = particle_pos;
		i = 0;
		while (i < nb_players && i < 50)
		{
			//if (player_percent[i] >= 0)
			final_pos += ratio_follow * (player_percent[i] / max_dist) * (mod_center(player_center[i]));
			i++;
		}
	}
	final_pos.x = moded(final_pos.x, map_size.x);
	final_pos.y = moded(final_pos.y, map_size.y);
	return (final_pos);
}

float	get_particle_size(float dist, float max_size)
{
	float	min_size;
	float	max_dist = 200.0f;

	float	p1 = 2.0f;
	float	p2 = 1.0f;
	float	ld = 0.2f;
	float	a;
	float	b;

	float	circle_size;

	if (skin_back == 0)
		min_size = 0.25f;
	else if (skin_back == 1)
		//min_size = -4.0f;
		min_size = 0.0f;		
	else
		min_size = 1.0f;

	ld = ld * max_size;
	min_size *= max_size;

	a = (max_size - ld) / pow(max_dist, p1);
	b = (max_size - a * pow(max_dist, p1)) * pow(max_dist, p2);

	if(dist <= max_dist)
	{
		circle_size = max(min_size, max_size - a * pow(dist, p1));
	}
	else
	{
		circle_size = max(min_size, b / pow(dist, p2));
	}


	//float	a = 0.8f * max_size / pow(max_dist, 2);
	//return max(min_size, max_size - a * pow(dist, 2));
	return circle_size;
}

void	main(void)
{
	vec2	closest;
	float	dist;
	float	max_size;
	vec2	final_pos;
	float	size;

	max_size = 0.9f * particle_size;
	final_pos = get_particle_pos(gl_in[0].gl_Position.xy);
	closest = get_closest_player(final_pos);
	dist = get_dist(final_pos, closest);

	size = get_particle_size(dist, max_size);
	out_size = size / max_size;

	pos = vec2(1.0f, -1.0f);
	emit_point(vec2(final_pos.x + size / 2.0f, final_pos.y - size / 2.0f));
	pos = vec2(-1.0f, -1.0f);
	emit_point(vec2(final_pos.x - size / 2.0f, final_pos.y - size / 2.0f));
	pos = vec2(1.0f, 1.0f);
	emit_point(vec2(final_pos.x + size / 2.0f, final_pos.y + size / 2.0f));
	pos = vec2(-1.0f, 1.0f);
	emit_point(vec2(final_pos.x - size / 2.0f, final_pos.y + size / 2.0f));
	EndPrimitive();
}
