# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bperreon <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/22 17:51:18 by bperreon          #+#    #+#              #
#    Updated: 2016/05/07 11:19:28 by bperreon         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = zatacka

UNAME = $(shell uname -s)

CC = clang
CFLAGS = -Wall -Wextra -Werror -O3

LIB_FOLDER = ./lib/
GLFW = ./glfw/src/libglfw3.a
GLEW = ./glew/build/cmake/lib/libglew32.a
FT_LOG = ./ft_log/ft_log.a
ARRAYLIST = ./arraylist/arraylist.a
LIB_COLORS = ./lib_colors/lib_colors.a
ifneq (,$(findstring CYGWIN,$(UNAME)))
	LIB = -L$(LIB_FOLDER) -lglfw3 -lgdi32 -lglew32 -lopengl32 \
		  $(LIB_FOLDER)arraylist.a $(LIB_FOLDER)ft_log.a $(LIB_FOLDER)lib_colors.a
else ifneq (,$(findstring Darwin,$(UNAME)))
	LIB = $(LIB_FOLDER)libglfw3.a -framework Cocoa -framework OpenGL \
		  -framework IOKit -framework CoreVideo \
		  $(LIB_FOLDER)arraylist.a $(LIB_FOLDER)ft_log.a $(LIB_FOLDER)lib_colors.a
endif
HEADERS = -I./glfw/include -I./glew/include/GL/ -I./include \
		  -I./ft_log/include -I./arraylist/include -I./lib_colors/include

SOURCE = src/main.c src/loop.c src/render.c src/update.c src/events.c \
		 src/init_env.c src/delete_env.c src/init_player.c src/load_players.c \
		 src/load_shader_program.c src/load_shader.c src/read_file.c \
		 src/check_error.c src/loaders.c src/flags.c src/add_indice.c \
		 src/vector.c src/collisions.c src/update_background.c \
		 src/update_player.c src/pop_a_point.c src/update_cam.c src/chunk.c \
		 src/init_background.c src/check_collisions.c src/init_cam.c \
		 src/get_player_color.c
		 
OBJ = $(SOURCE:.c=.o)

# ALL

ifneq (,$(findstring CYGWIN,$(UNAME)))
all: $(LIB_FOLDER) $(GLFW) $(GLEW) $(FT_LOG) $(ARRAYLIST) $(LIB_COLORS) $(NAME)
else ifneq (,$(findstring Darwin,$(UNAME)))
all: $(LIB_FOLDER) $(GLFW) $(FT_LOG) $(ARRAYLIST) $(LIB_COLORS) $(NAME)
else
all: ERROR
endif

ERROR:
	@echo "no rules for make on this os"

$(NAME): $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(LIB)

%.o: %.c include/*.h
	$(CC) $(CFLAGS) $(HEADERS) -c $< -o $@

# LIB

$(LIB_FOLDER):
	mkdir $@ -p

$(FT_LOG):
	git submodule init ft_log
	git submodule update ft_log
	make -C ft_log
	cp $(FT_LOG) $(LIB_FOLDER)

$(ARRAYLIST):
	git submodule init arraylist
	git submodule update arraylist
	make -C arraylist
	cp $(ARRAYLIST) $(LIB_FOLDER)

$(LIB_COLORS):
	git submodule init lib_colors
	git submodule update lib_colors
	make -C lib_colors
	cp $(LIB_COLORS) $(LIB_FOLDER)

$(GLFW):
	git submodule init glfw
	git submodule update glfw
	cd glfw && cmake . -DCMAKE_LEGACY_CYGWIN_WIN32=1 && make -j 8 -i && cd ..
	cp $(GLFW) $(LIB_FOLDER)

$(GLEW):
	git submodule init glew
	git submodule update glew
	make -C glew/auto
	mv glew/src/glew.c glew/src/glew_old.c
	echo "#define _WIN32" > glew/src/glew.c
	cat glew/src/glew_old.c >> glew/src/glew.c
	rm glew/src/glew_old.c
	cd glew/build/cmake && cmake . -DCMAKE_LEGACY_CYGWIN_WIN32=1 && make -j 8 -i && cd ../../..
	cp $(GLEW) $(LIB_FOLDER)

# CLEAN - FCLEAN - RE

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all
