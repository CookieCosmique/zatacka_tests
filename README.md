# README #

### What is this repository for? ###

* Tests for game Zatacka in C using OpenGL
* Gamed based on original Zatacka and Curve fever games. Each player move and arrow that draw a line behind it. When the arrow collides with any section of line, the player dies. In this program the player will bounce and not die.
* More options, Weapons, Bonuses, Maps, Game modes and online mode will be added in the C++ version

### How do I get set up? ###

* Works on environments Cygwin and Darwin
* Makefile
* If libglfw3.a or libglew32.a does not work, download it on the official website

### How do i execute the game ? ###

* /Zatacka [-abdn] nb
* **arguments** :
* nb = number of players
*   a : anti-aliasing
*   b : disable background
*   d : more debug messages
*   n : neon mode
* **controls** :
*   Player 1 : arrows
*   Player 2 : A,W,D (qwerty)
*   Player 3 : H,U,K
*   Camera: Change mode: 1(manual),2(semi-lock),3(follow) Manual: 4,8,6,2,+,- (numpad)
*   Time: L(/2),M(x2) (qwerty) P(pause/play)
*   Quit: Escape

### Videos ###

* https://youtu.be/VGBl3iBsnWk
* https://youtu.be/ix4CRHfhxVc

### Screenshots ###

![share.png](https://bitbucket.org/repo/Lg9zyj/images/3708824248-share.png)
![Share2.png](https://bitbucket.org/repo/Lg9zyj/images/1390157473-Share2.png)
![Share3.png](https://bitbucket.org/repo/Lg9zyj/images/529936617-Share3.png)
![share4.png](https://bitbucket.org/repo/Lg9zyj/images/3809225956-share4.png)