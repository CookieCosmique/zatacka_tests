/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/21 15:07:58 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/21 15:07:58 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FLAGS_H
# define FLAGS_H

# define NB_FLAGS 5

enum			e_flags
{
	F_DEBUG = 1,
	F_ANTI_ALIASING = 2,
	F_NO_BACKGROUND = 4,
	F_NEON = 8,
	F_BACKGROUND_DEFORME = 16,
};

typedef struct	s_flag
{
	const int	flag;
	const char	c;
}				t_flag;

int				get_flags(int ac, char **av);

#endif