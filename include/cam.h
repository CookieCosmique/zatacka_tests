/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cam.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/21 15:07:58 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/09 14:38:53 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CAM_H
# define CAM_H

# define CAM_INPUT_NB 6
# define CAM_TYPE_NB 3

enum			e_cam_move
{
	CAM_MOVE_LEFT = 1,
	CAM_MOVE_RIGHT = 2,
	CAM_MOVE_UP = 4,
	CAM_MOVE_DOWN = 8,
	CAM_ZOOM_IN = 16,
	CAM_ZOOM_OUT = 32
};

typedef struct	s_cam_input
{
	int		*keys;
	int		moves;
}				t_cam_input;

typedef struct	s_cam_type
{
	void	(*update)(struct s_cam_type *cam, t_player *p, float *cam_pos,
			float *cam_size, const float t, float *translate);
	int		key;
	int		pressed;

	float	*pos;
	float	*size;
	float	ratio;
	float	range;
}				t_cam_type;

typedef struct	s_cam
{
	size_t		status;
	t_cam_type	**cam_type;

	float		*size;
	float		*pos;

	t_cam_input	input;
	float		speed;
	float		zoom_speed;

	float		translate[2];

	float		*trans_start_pos;
	float		*trans_curr_pos;
	float		*trans_dir;
	float		trans_duration;
	float		trans_dir_size;
}				t_cam;

void			update_manual_cam(t_cam_type *cam, t_player *p, float *cam_pos,
			float *cam_size, const float t, float *translate);
void			update_follow_cam(t_cam_type *cam, t_player *p, float *cam_pos,
			float *cam_size, const float t, float *translate);
void			update_yolo_cam(t_cam_type *cam, t_player *p, float *cam_pos,
			float *cam_size, const float t, float *translate);

#endif
