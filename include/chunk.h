/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shader.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/21 15:07:58 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/21 15:07:58 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHUNK_H
# define CHUNK_H

#include "zatacka.h"
#include "array_list.h"

typedef struct		s_line
{
	GLuint			index[2];
	size_t			player;
}					t_line;

typedef struct		s_chunk
{
	t_alist			*lines;
}					t_chunk;

typedef struct		s_group_chunk
{
	float 			center[2];
	float			up_left[2];
	float 			height;
	float 			width;
	float			chunk_w;
	float			chunk_h;

	int 			max_depth;
	t_chunk 		*chunks;
	size_t			size_all;
	size_t			size;
}					t_group_chunk;

typedef	struct 		s_pack_chunk
{
	size_t			depth;
	t_chunk			*ul_chunk;
	t_group_chunk	*grp;
}					t_pack_chunk;

typedef struct		s_quad_obj
{
	float			q_pos[8];
	float			q_up_left[8];
	float			q_down_right[8];
	t_chunk			*q_chunk[4];

	t_group_chunk	*grp;
}					t_quad_obj;

t_group_chunk	*create_chunk_group(float width, float height, float center_x, float center_y, int max_depth);

t_chunk			*right_(t_chunk *curr, t_group_chunk *grp);
t_chunk			*left_(t_chunk *curr, t_group_chunk *grp);
t_chunk			*up_(t_chunk *curr, t_group_chunk *grp);
t_chunk			*down_(t_chunk *curr, t_group_chunk *grp);

t_chunk			*right_n(t_chunk *curr, t_group_chunk *grp, size_t n);
t_chunk			*left_n(t_chunk *curr, t_group_chunk *grp, size_t n);
t_chunk			*up_n(t_chunk *curr, t_group_chunk *grp, size_t n);
t_chunk			*down_n(t_chunk *curr, t_group_chunk *grp, size_t n);

float			*up_left_of_chunk(float *pos, t_chunk *curr, t_group_chunk *grp);

t_chunk 		*find_chunk_point(float *point, t_group_chunk *grp);
t_pack_chunk 	*find_pack_chunk_poly(float *point, size_t nbr, t_group_chunk *grp);
t_chunk			*update_chunk_point(t_chunk *curr, float *new_point, t_group_chunk *grp);

t_quad_obj		*create_quad_obj(float *pos_center, float width, float height,t_group_chunk *grp, void (*func) (t_chunk *chunk, void *data), void *data);
void			update_quad(float *dep, t_quad_obj *q_obj, void (*func) (t_chunk *chunk, void *data), void *data);
void			recal_quad_on_curr_chunk(t_quad_obj *q_obj, void (*func) (t_chunk *chunk, void *data), void *data);

void			add_line_to_quad(t_chunk *chunk, void *data);

size_t			row_(t_chunk *curr, t_group_chunk *grp);
size_t			col_(t_chunk *curr, t_group_chunk *grp);

void			print_lines_in_chunk(t_chunk *chunk);

#endif
