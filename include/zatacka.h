/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   zatacka.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/21 15:07:58 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/09 10:29:28 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZATACKA_H
# define ZATACKA_H

# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>
# include <errno.h>
# include <time.h>

# include "ft_log.h"
# include "lib_colors.h"

# include "opengl.h"
# include "player.h"
# include "flags.h"
# include "cam.h"
# include "chunk.h"
# include "background.h"

# define RATIO (16.0f / 9.0f)
# define HEIGHT 720
# define WIDTH (RATIO * HEIGHT)

# define FULLSCREEN 0
# define MSAA 8

typedef struct	s_map
{
	size_t		width;
	size_t		height;
}				t_map;

typedef struct	s_frame_buffer
{
	GLuint		msaa_buf;
	GLuint		msaa_tex;
	GLuint		rbo_depth;
}				t_frame_buffer;

typedef struct	s_env
{
	GLFWwindow		*window;
	t_alist			*players;

	t_background	background;

	t_map			map;
	t_cam			*cam;

	t_player_sp		player_sp;
	t_line_sp		line_sp;

	t_frame_buffer	frame_buffer;

	t_group_chunk	*group_chunk;

	float			game_speed;
	int				pause;
	double			t;
	int				flags;
}				t_env;

t_env			*init_env(int ac, char **av);
void			delete_env(t_env **e);

int				loop(t_env *e);
int				events(t_env *e);
int				update(t_env *e);
int				render(t_env *e);

int				load_players(t_env *e, int ac, char **av);

int				init_background(t_background *b, const size_t nb_players,
		t_map map);

int				init_cam(t_env *e, const float width, const float height);
void			update_cam(t_env *e, const float t, const float game_speed);

#endif
