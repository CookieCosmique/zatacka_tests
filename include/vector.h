/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/21 15:07:58 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/21 15:07:58 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VECTOR_H
# define VECTOR_H

# include <math.h>
# include <stdlib.h>

float			*vec2(float const x, float const y);

float			vec2_norm(float *vec);
float			*vec2_normalize(float *vec);
float			*vec2_rotate(float *vec, const float angle);

#endif