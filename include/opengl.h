/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   opengl.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/21 15:07:58 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/21 15:07:58 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPENGL_H
# define OPENGL_H

# ifdef __APPLE__
# 	define GLFW_INCLUDE_GLCOREARB
# endif
# if defined(_WIN32) || defined(__CYGWIN__)
# 	include <windef.h>
#	define GLEW_STATIC
# 	include "GL/glew.h"
# endif

# include <ctype.h>
# include "GLFW/glfw3.h"

GLuint				load_vbo(const GLuint program_id, GLsizeiptr size,
		const GLvoid *data, const GLuint index, const char *name, GLint nb, GLenum usage);
GLuint				load_vbo_tf(GLsizeiptr size, const GLvoid *data);
GLuint				load_ebo(GLsizeiptr size, const GLvoid *data);

unsigned int		check_error(void);

#endif

