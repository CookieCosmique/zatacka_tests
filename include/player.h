/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/21 15:07:58 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/09 15:25:40 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PLAYER_H
# define PLAYER_H

# include "chunk.h"
# include "shader.h"
# include "vector.h"
# include "array_list.h"
# include "collisions.h"

# define PLAYER_INPUT_NB 3

enum			e_moves
{
	PLAYER_ROTATION_LEFT = 1,
	PLAYER_ROTATION_RIGHT = 2,
	PLAYER_SPEED = 4
};

typedef struct	s_player_input
{
	int		keys[PLAYER_INPUT_NB];
	int		moves;
}				t_player_input;

typedef struct	s_player
{
	t_player_sp		*player_sp;
	t_line_sp		*line_sp;

	t_alist			*lines;
	t_alist			*indices;

	GLuint			vao_pos;
	GLuint			vbo_pos;
	GLuint			vbo_dir;

	GLuint			vao_lines;
	GLuint			vbo_lines;
	GLuint			ebo_lines;

	int				dead;
	int				draw;
	float			time_draw;

	float			*pos;
	float			*dir;

	float			to_ignore;
//	float			ignore_lines;

	float			rotation_speed;
	float			speed;
	float			speed_min;
	float			speed_max;

//	float			free_to;

	unsigned int	color;
	unsigned int	size_line;
	unsigned int	size_arrow;

	t_player_input	input;

	t_line			last_line;

	int				skin_line;
	int				skin_arrow;
	int				skin_back;

	t_quad_obj		*player_q_obj;
	t_quad_obj		*line_q_obj;

}				t_player;

int				get_player_color(const int nb);
t_player		*init_player(const int color, const int skin,
			t_player_sp *player_sp, t_line_sp *line_sp, float *pos, t_group_chunk *grp, size_t num);

t_alist			*add_indice(t_alist *indices, const GLuint value);
void 			pop_a_point(t_player *p);
void			start_line(t_player *p);
void			stop_line(t_player *p);

void			update_player(const size_t player_id, const double t, t_alist *players);

float			*check_collisions(float *p1, float *p2, float *norm_col, t_player **all_player, size_t num);

#endif
