/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   collisions.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/21 15:07:58 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/02 11:56:07 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COLLISIONS_H
# define COLLISIONS_H

int				collision_rectangles(float *r1, float *r2);
int				collision_rectangle_polygon(float *rect, float *p, size_t nb);
int				collision_polygons(float *p1, size_t n1, float *p2, size_t n2);
int				collision_point_polygon(float *point, float *polygon, size_t nb);

#endif
