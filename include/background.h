/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   background.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/21 15:07:58 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/02 11:44:34 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BACKGROUND_H
# define BACKGROUND_H

# include "shader.h"
# include "array_list.h"

typedef struct		s_background
{
	t_background_sp		background_sp;

	GLuint				vao;
	GLuint				vbo_particle_pos;

	size_t				nb_particles;
	size_t				particle_size;
	float				*particle_pos;
	float				*players_pos;
	float				*players_center;
}					t_background;


void				update_background(t_background b, t_alist *players,
		const double t);

#endif
