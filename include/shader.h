/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shader.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/21 15:07:58 by bperreon          #+#    #+#             */
/*   Updated: 2016/03/02 11:27:37 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHADER_H
# define SHADER_H

# include "opengl.h"

# define MAX_PLAYER 30
typedef struct	s_shader_program
{
	GLuint		id;

	GLuint		vertex_shader;
	GLuint		geometry_shader;
	GLuint		fragment_shader;
}				t_shader_program;

typedef struct	s_player_sp
{
	t_shader_program	shader_program;

	GLuint				id_color;
	GLuint				id_speed;
	GLuint				id_size;

	GLuint				id_skin_arrow;
	GLuint				id_skin_back;

	GLuint				id_cam_size;
	GLuint				id_cam_pos;
}				t_player_sp;

typedef struct	s_line_sp
{
	t_shader_program	shader_program;

	GLuint				id_color;
	GLuint				id_size;
	GLuint				id_last_vert;

	GLuint				id_skin_line;
	GLuint				id_skin_back;

	GLuint				id_cam_size;
	GLuint				id_cam_pos;
}				t_line_sp;

typedef struct	s_background_sp
{
	t_shader_program	shader_program;

	GLuint				id_cam_size;
	GLuint				id_cam_pos;
	GLuint				id_map_size;

	GLuint				id_skin_back;

	GLuint				id_player_pos;
	GLuint				id_player_center;
	GLuint				id_nb_players;

	GLuint				id_particle_size;

}				t_background_sp;

char			*read_file(const char *path);

GLuint			load_shader(const char *path, GLenum shader_type);
int				load_player_shader_program(t_player_sp *p);
int				load_line_shader_program(t_line_sp *p);
int				load_background_shader_program(t_background_sp *p);

#endif
